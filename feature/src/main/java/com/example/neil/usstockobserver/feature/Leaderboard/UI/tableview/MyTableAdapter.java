package com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evrencoskun.tableview.adapter.AbstractTableAdapter;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractSorterViewHolder;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;
import com.example.neil.usstockobserver.feature.Leaderboard.Data.User;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.holder.CellViewHolder;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.holder.ColumnHeaderViewHolder;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.holder.RowHeaderViewHolder;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.model.CellModel;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.model.ColumnHeaderModel;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.model.RowHeaderModel;
import com.example.neil.usstockobserver.feature.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evrencoskun on 27.11.2017.
 */

public class MyTableAdapter extends AbstractTableAdapter<ColumnHeaderModel, RowHeaderModel,
        CellModel> {

    private MyTableViewModel myTableViewModel;

    public MyTableAdapter(Context p_jContext) {
        super(p_jContext);

        this.myTableViewModel = new MyTableViewModel();
    }


    @Override
    public AbstractViewHolder onCreateCellViewHolder(ViewGroup parent, int viewType) {
        return new CellViewHolder(LayoutInflater.from(mContext).inflate(R.layout.tableview_cell_layout,
                parent, false));
    }

    @Override
    public void onBindCellViewHolder(AbstractViewHolder holder, Object p_jValue, int
            p_nXPosition, int p_nYPosition) {
        CellModel cell = (CellModel) p_jValue;
        ((CellViewHolder) holder).setCellModel(cell, p_nXPosition);

    }

    @Override
    public AbstractSorterViewHolder onCreateColumnHeaderViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(mContext).inflate(R.layout
                .tableview_column_header_layout, parent, false);

        return new ColumnHeaderViewHolder(layout, getTableView());
    }

    @Override
    public void onBindColumnHeaderViewHolder(AbstractViewHolder holder, Object p_jValue, int
            p_nXPosition) {
        ColumnHeaderModel columnHeader = (ColumnHeaderModel) p_jValue;

        // Get the holder to update cell item text
        ColumnHeaderViewHolder columnHeaderViewHolder = (ColumnHeaderViewHolder) holder;

        columnHeaderViewHolder.setColumnHeaderModel(columnHeader, p_nXPosition);
    }

    @Override
    public AbstractViewHolder onCreateRowHeaderViewHolder(ViewGroup parent, int viewType) {

        // Get Row Header xml Layout
        View layout = LayoutInflater.from(mContext).inflate(R.layout.tableview_row_header_layout,
                parent, false);

        // Create a Row Header ViewHolder
        return new RowHeaderViewHolder(layout);
    }

    @Override
    public void onBindRowHeaderViewHolder(AbstractViewHolder holder, Object p_jValue, int
            p_nYPosition) {

        RowHeaderModel rowHeaderModel = (RowHeaderModel) p_jValue;

        RowHeaderViewHolder rowHeaderViewHolder = (RowHeaderViewHolder) holder;
        rowHeaderViewHolder.row_header_textview.setText(rowHeaderModel.getData());

    }

    @Override
    public View onCreateCornerView() {
        return LayoutInflater.from(mContext).inflate(R.layout.tableview_corner_layout, null, false);
    }

    @Override
    public int getColumnHeaderItemViewType(int position) {
        return 0;
    }

    @Override
    public int getRowHeaderItemViewType(int position) {
        return 0;
    }

    @Override
    public int getCellItemViewType(int position) {
        return myTableViewModel.getCellItemViewType(position);
    }

    /**
     * This method is not a generic Adapter method. It helps to generate lists from whole stock data set
     * list for this adapter.
     */
    public String findStockSymbolByValue(String value) {
        List<List<CellModel>> mCellItems = getCellRecyclerViewAdapter().getItems();

        for (int i = 0; i < mCellItems.size(); i++) {
            List<CellModel> row = mCellItems.get(i);

            for (int j = 0; j < row.size(); j++) {
                CellModel item = row.get(j);
                if (item.getData().equals(value)) {
                    return item.getStockSymbol();
                }
            }
        }
        return "";
    }

    /**
     * This method is not a generic Adapter method. It helps to generate lists from whole stock data set
     * list for this adapter.
     */
    public void setWholeStockDataSet(ArrayList<ArrayList<String>> wholeStockDataSet) {
        // Generate the lists that are used to TableViewAdapter
        myTableViewModel.generateWholeStockListForTableView(wholeStockDataSet);

        // Now we got what we need to show on TableView.
        setAllItems(myTableViewModel.getColumHeaderModeList(), myTableViewModel
                .getRowHeaderModelList(), myTableViewModel.getCellModelList());
    }

    /**
     * This method is not a generic Adapter method. It helps to generate lists from single user
     * list for this adapter.
     */
    public void setUserList(List<User> userList) {
        // Generate the lists that are used to TableViewAdapter
        myTableViewModel.generateListForTableView(userList);

        // Now we got what we need to show on TableView.
        setAllItems(myTableViewModel.getColumHeaderModeList(), myTableViewModel
                .getRowHeaderModelList(), myTableViewModel.getCellModelList());
    }

}
