package com.example.neil.usstockobserver.feature.Leaderboard;

import android.content.Context;

import com.example.neil.usstockobserver.feature.Leaderboard.Data.network.UserNetworkDataSource;
import com.example.neil.usstockobserver.feature.Leaderboard.Data.network.UserRepository;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.viewmodel.MainViewModelFactory;
import com.example.neil.usstockobserver.feature.SharedViewModelFactory;

public class InjectorUtils {

    public static UserRepository getRepository(Context context) {
        // Get all we need
        //UserDao userDao = UserDatabase.getInstance(context).userDao();
        AppExecutors executors = AppExecutors.getInstance();
        UserNetworkDataSource networkDataSource = UserNetworkDataSource.getInstance(executors);

        return UserRepository.getInstance(networkDataSource, executors);
    }

    public static MainViewModelFactory getMainViewModelFactory(Context context){
        UserRepository repository = getRepository(context);
        return new MainViewModelFactory(repository);
    }

    public static SharedViewModelFactory getSharedViewModelFactory(Context context) {
        UserRepository repository = getRepository(context);
        return new SharedViewModelFactory(repository);
    }
}
