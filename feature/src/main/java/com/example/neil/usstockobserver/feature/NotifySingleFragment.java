package com.example.neil.usstockobserver.feature;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotifySingleFragment extends Fragment {


    public NotifySingleFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notify_single, container, false);

//        TableLayout table = (TableLayout)getView().findViewById(R.id.notify_table);
//        for(ResourceBalance b : xmlDoc.balance_info)
//        {
//            // Inflate your row "template" and fill out the fields.
//            TableRow row = (TableRow)LayoutInflater.from(getView()).inflate(R.layout.tb_row, null);
//            ((TextView)row.findViewById(R.id.attrib_name)).setText(b.NAME);
//            ((TextView)row.findViewById(R.id.attrib_value)).setText(b.VALUE);
//            table.addView(row);
//        }
//        table.requestLayout();
    }


}
