package com.example.neil.usstockobserver.feature.Leaderboard;

public final class ServiceParamUtils {
    public static final String SingleStock_Action = "getdtnodata";
    public static final String WholeStock_Action = "getdtnodata";
    public static final int SingleStock_Daily_DtNo = 4507402;
    public static final int SingleStock_Weekly_DtNo = 4507408;
    public static final int SingleStock_Quartly_DtNo = 4507409;
    public static final int SingleStock_Info_DtNo = 4507420;
    public static final int WholeStock_Info_DtNo = 4511001;
    public static final String SingleStock_Daily_ParamStr = "DTMode=0;DTRange=250;DTOrder=2;MajorTable=M783;MTPeriod=0;AssignID=";
    public static final String SingleStock_Weekly_ParamStr = "DTMode=0;DTRange=60;DTOrder=2;MajorTable=M784;MTPeriod=0;AssignID=";
    public static final String SingleStock_Quartly_ParamStr = "DTMode=0;DTRange=60;DTOrder=2;MajorTable=M712;MTPeriod=3;AssignID=";
    public static final String SingleStock_Info_ParamStr = "DTMode=0;DTRange=1;DTOrder=1;MajorTable=M682;MTPeriod=4;AssignID=";
    public static final String WholeStock_ParamStr = "SPMode=0;DTMode=0;";
    public static final int SingleStock_FilterNo = 0;
    public static final int WholeStock_FilterNo = 0;
    public static final String SingleStock_KeyMap = "";
    public static final String WholeStock_KeyMap = "";
    public static final String SingleStock_AssignSPID = "";
    public static final String WholeStock_AssignSPID = "";
    public static final String StockServiceUrl = "https://www.cmoney.tw/MobileService/ashx/";

    //公司資料
    public static final int SingleStock_CompanyInfo_DtNo = 4005339;
    public static final String SingleStock_Company_info_ParamStr = "DTMode=0;DTRange=1;DTOrder=1;MajorTable=M712;MTPeriod=3;AssignID=";


    public static String getCompanyInfoStrParam(int range) {
        String result = new StringBuilder().append("DTMode=0;DTRange=").append(range).append(";DTOrder=1;MajorTable=M712;MTPeriod=3;AssignID=").toString();
        return result;
    }
}
