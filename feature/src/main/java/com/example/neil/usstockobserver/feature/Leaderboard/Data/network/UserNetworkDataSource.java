package com.example.neil.usstockobserver.feature.Leaderboard.Data.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.example.neil.usstockobserver.feature.Leaderboard.AppExecutors;
import com.example.neil.usstockobserver.feature.Leaderboard.Data.User;
import com.example.neil.usstockobserver.feature.Leaderboard.model.ServiceRequest;
import com.example.neil.usstockobserver.feature.Leaderboard.model.ServiceResponse;
import com.example.neil.usstockobserver.feature.SingleStockDataResponse;
import com.example.neil.usstockobserver.feature.StockItem;
import com.example.neil.usstockobserver.feature.WholeStockResponse;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.DisposableObserver;

public class UserNetworkDataSource {

    private static final String LOG_TAG = UserNetworkDataSource.class.getSimpleName();

    // For Singleton instantiation
    private static UserNetworkDataSource sInstance;
    private static final Object LOCK = new Object();

    private AppExecutors mAppExecutors;
    private final MutableLiveData<List<User>> mDownloadedData;
    private final MutableLiveData<ArrayList<ArrayList<String>>> mDownloadedWholeStockDataSet;


    private final MutableLiveData<ArrayList<StockItem>> mDownloadedWholeWatchedItems;

    public UserNetworkDataSource(AppExecutors mAppExecutors) {
        this.mAppExecutors = mAppExecutors;
        this.mDownloadedData = new MutableLiveData<>();
        this.mDownloadedWholeStockDataSet = new MutableLiveData<>();
        this.mDownloadedWholeWatchedItems = new MutableLiveData<>();
    }

    public static UserNetworkDataSource getInstance(AppExecutors executors) {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new UserNetworkDataSource(executors);
                Log.d(LOG_TAG, "Made new network data source");
            }
        }
        return sInstance;
    }

    public void fetchData(final ServiceRequest serviceRequest) {
        mAppExecutors.networkIO().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    NetworkUtils.getDataFromService(serviceRequest.getSize(), serviceRequest.getPage(), new
                            DisposableObserver<ServiceResponse>() {

                                @Override
                                public void onNext(ServiceResponse serviceResponse) {
                                    setUserList(NetworkUtils.convertToUserList(serviceResponse));
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.e(LOG_TAG, "Getting data process has been failed.", e);
                                }

                                @Override
                                public void onComplete() {

                                }
                            });

                } catch (Exception ex) {
                    Log.e(LOG_TAG, "Getting data process has been failed.", ex);
                }
            }
        });
    }

    public void fetchWholeStockData() {
        mAppExecutors.networkIO().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    NetworkUtils.getWholeStockDataFromService(new DisposableObserver<WholeStockResponse>() {

                        @Override
                        public void onNext(WholeStockResponse wholeStockResponse) {
                            setWholeStockDataSet(NetworkUtils.convertToStockDataSet(wholeStockResponse));
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(LOG_TAG, "Getting data process has been failed.", e);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });

                } catch (Exception ex) {
                    Log.e(LOG_TAG, "Getting data process has been failed.", ex);
                }
            }
        });
    }

    public void fetchSavedStockItems(final ArrayList<String> savedStockItems) {
        mAppExecutors.networkIO().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    NetworkUtils.getMultipleStockItems(savedStockItems, new DisposableObserver<ArrayList<SingleStockDataResponse>>() {
                        @Override
                        public void onNext(ArrayList<SingleStockDataResponse> singleStockDataResponses) {
                            setWholeWatchedItems(NetworkUtils.convertToWatchedItems(singleStockDataResponses));
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });

                } catch (Exception ex) {
                    Log.e(LOG_TAG, "Getting data process has been failed.", ex);
                }
            }
        });
    }

    private void setWholeStockDataSet(ArrayList<ArrayList<String>> wholeStockDataSet) {
        mDownloadedWholeStockDataSet.postValue(wholeStockDataSet);
    }

    private void setWholeWatchedItems(ArrayList<StockItem> watchedItems) {
        mDownloadedWholeWatchedItems.postValue(watchedItems);
    }

    public LiveData<ArrayList<ArrayList<String>>> getWholeStockDataSet() {
        return mDownloadedWholeStockDataSet;
    }

    public LiveData<ArrayList<StockItem>> getDownloadedWholeWatchedItems() {
        return mDownloadedWholeWatchedItems;
    }

    private void setUserList(List<User> userList){
        mDownloadedData.postValue(userList);
    }

    public LiveData<List<User>> getUserList(){
        return mDownloadedData;
    }


}
