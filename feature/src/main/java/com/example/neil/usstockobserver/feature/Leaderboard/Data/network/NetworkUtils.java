package com.example.neil.usstockobserver.feature.Leaderboard.Data.network;

import android.util.Log;

import com.example.neil.usstockobserver.feature.Leaderboard.Data.User;
import com.example.neil.usstockobserver.feature.Leaderboard.Data.network.pojo.Data;
import com.example.neil.usstockobserver.feature.Leaderboard.model.ServiceResponse;
import com.example.neil.usstockobserver.feature.SingleStockDataResponse;
import com.example.neil.usstockobserver.feature.StockItem;
import com.example.neil.usstockobserver.feature.WholeStockResponse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.SingleStock_Action;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.SingleStock_AssignSPID;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.SingleStock_CompanyInfo_DtNo;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.SingleStock_Company_info_ParamStr;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.SingleStock_FilterNo;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.SingleStock_KeyMap;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.StockServiceUrl;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.WholeStock_Action;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.WholeStock_AssignSPID;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.WholeStock_FilterNo;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.WholeStock_Info_DtNo;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.WholeStock_KeyMap;
import static com.example.neil.usstockobserver.feature.Leaderboard.ServiceParamUtils.WholeStock_ParamStr;


public class NetworkUtils {

    //TODO: Put  Connections Param here
    private static final String LOG_TAG = NetworkUtils.class.getSimpleName();
    private static final String BASE_URL = "https://vuetable.ratiw.net/api/";

    private static Retrofit getRetrofit() {
        return new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory
                .create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();
    }

    private static Retrofit getRetrofit(String url) {
        return new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory
                .create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();
    }

    //TODO: Refactor into our API
    public static Disposable getWholeStockDataFromService(DisposableObserver<WholeStockResponse> observer) {
        Log.d(LOG_TAG, "Getting whole stock data from the server");
        try {
            RestApi service = getRetrofit(StockServiceUrl).create(RestApi.class);

            Observable<WholeStockResponse> observable = service.getWholeStock(
                    WholeStock_Action, WholeStock_Info_DtNo, WholeStock_ParamStr, WholeStock_FilterNo, WholeStock_KeyMap, WholeStock_AssignSPID
            );
            return observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread
                    ()).subscribeWith(observer);
        } catch (Exception e) {
            Log.d(LOG_TAG, "Getting data process has been failed. ", e);
        }
        return null;
    }

    public static Disposable getSingletStockItem(String stockSymbol,
                                                 DisposableObserver<SingleStockDataResponse> observer) {
        Log.d(LOG_TAG, "Getting whole stock data from the server");
        try {
            RestApi service = getRetrofit(StockServiceUrl).create(RestApi.class);

            Observable<SingleStockDataResponse> observable = service.getSingleStock(
                    SingleStock_Action, SingleStock_CompanyInfo_DtNo, SingleStock_Company_info_ParamStr + stockSymbol, SingleStock_FilterNo, SingleStock_KeyMap, SingleStock_AssignSPID
            );
            return observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread
                    ()).subscribeWith(observer);
        } catch (Exception e) {
            Log.d(LOG_TAG, "Getting data process has been failed. ", e);
        }
        return null;
    }

    public static Disposable getMultipleStockItems(ArrayList<String> savedStockList,
                                                   DisposableObserver<ArrayList<SingleStockDataResponse>> observer) {
        Log.d(LOG_TAG, "Getting whole stock data from the server");
        try {
            ArrayList<Observable<SingleStockDataResponse>> requests = new ArrayList<>();
            RestApi service = getRetrofit(StockServiceUrl).create(RestApi.class);
            for (int i = 0; i < savedStockList.size(); i++) {
                Observable<SingleStockDataResponse> observable = service.getSingleStock(
                        SingleStock_Action, SingleStock_CompanyInfo_DtNo, SingleStock_Company_info_ParamStr + savedStockList.get(i), SingleStock_FilterNo, SingleStock_KeyMap, SingleStock_AssignSPID
                );
                requests.add(observable);
            }

            Observable zip = Observable.zip(requests, new io.reactivex.functions.Function<Object[], List<SingleStockDataResponse>>() {
                @Override
                public List<SingleStockDataResponse> apply(Object[] objects) {
                    List<SingleStockDataResponse> resultList = new ArrayList<>();
                    for (int i = 0; i < objects.length; i++) {
                        resultList.add((SingleStockDataResponse) objects[i]);
                    }
                    return resultList;
                }
            });
            return (Disposable) zip.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread
                    ()).subscribeWith(observer);
        } catch (Exception e) {
            Log.d(LOG_TAG, "Getting data process has been failed. ", e);
        }
        return null;
    }


    public static Disposable getDataFromService(int size, int page,
                                                DisposableObserver<ServiceResponse> observer) {
        Log.d(LOG_TAG, "Getting data from the server");
        try {
            RestApi service = getRetrofit().create(RestApi.class);

            Observable<ServiceResponse> observable = service.getUser(size, page);
            return observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread
                    ()).subscribeWith(observer);

        } catch (Exception e) {
            Log.d(LOG_TAG, "Getting data process has been failed. ", e);
        }
        return null;
    }

    //TODO: Json Response Parsing logic
    public static ArrayList<ArrayList<String>> convertToStockDataSet(WholeStockResponse wholeStockResponse) {
        ArrayList<String> titles = wholeStockResponse.getTitle();
        ArrayList<ArrayList<String>> dataSet = wholeStockResponse.getDataSet();
        ArrayList<ArrayList<String>> filteredDataSet = parseWholeStockData(titles, dataSet);

        return filteredDataSet;
    }

    public static ArrayList<StockItem> convertToWatchedItems(ArrayList<SingleStockDataResponse> singleStockDataResponseArrayList) {
        ArrayList<StockItem> stockItems = new ArrayList<>();
        for (int i = 0; i < singleStockDataResponseArrayList.size(); i++) {
            SingleStockDataResponse singleStockDataResponse = singleStockDataResponseArrayList.get(i);
            StockItem item = convertToStockItem(singleStockDataResponse);
            stockItems.add(item);
        }
        return stockItems;
    }

    public static StockItem convertToStockItem(SingleStockDataResponse singleStockDataResponse) {
        String stockSymbol = singleStockDataResponse.getDataSet().get(0).get(2);
        String stockName = singleStockDataResponse.getDataSet().get(0).get(3);
        String dailyClosedPrice = singleStockDataResponse.getDataSet().get(0).get(7);
        String sector = singleStockDataResponse.getDataSet().get(0).get(23);
        StockItem stockItem = new StockItem(stockName, stockSymbol, dailyClosedPrice, sector);
        return stockItem;
    }

    private static ArrayList<ArrayList<String>> parseWholeStockData(ArrayList<String> titles, ArrayList<ArrayList<String>> dataSet) {
        final int index_name = 0;
        final int index_TickerSymbols = 1;
        final int index_LS1_Close_Price = 2;
        final int index_LS2_Close_Price = 3;
        final int index_LS3_Close_Price = 4;
        final int index_LS4_Close_Price = 5;
        final int index_LS5_Close_Price = 6;
        final int index_LS1_delta_Price = 7;
        final int index_LS2_delta_Price = 8;
        final int index_LS3_delta_Price = 9;
        final int index_LS4_delta_Price = 10;
        final int index_LS5_delta_Price = 11;
        final int index_LY1_Close_Price = 12;
        final int index_LY2_Close_Price = 13;
        final int index_LY3_Close_Price = 14;
        final int index_LY1_delta_Price = 15;
        final int index_LY2_delta_Price = 16;
        final int index_LY3_delta_Price = 17;
        final int index_Sectored_Category_Id_Tier1 = 18;
        final int index_Sectored_Category_Name_Tier1 = 19;
        final int index_Sectored_Category_Name_ZH_Tier1 = 20;
        final int index_Sectored_Category_Id_Tier2 = 21;
        final int index_Sectored_Category_Tier2 = 22;
        final int index_Sectored_Category_Name_ZH_Tier2 = 23;
        final int index_LS1_GPM = 24;
        final int index_LS2_GPM = 25;
        final int index_LS3_GPM = 26;
        final int index_LS1_Net_Profit_Margin = 27;
        final int index_LS2_Net_Profit_Margin = 28;
        final int index_LS3_Net_Profit_Margin = 29;
        final int index_LS1_Operating_Profit_Margin = 30;
        final int index_LS2_Operating_Profit_Margin = 31;
        final int index_LS3_Operating_Profit_Margin = 32;
        final int index_LS1_ROE = 33;
        final int index_LS2_ROE = 34;
        final int index_LS3_ROE = 35;
        final int index_LS1_Current_Ratio = 36;
        final int index_LS2_Current_Ratio = 37;
        final int index_LS3_Current_Ratio = 38;
        final int index_LS1_Inventory_Turnover = 39;
        final int index_LS2_Inventory_Turnover = 40;
        final int index_LS3_Inventory_Turnover = 41;
        final int index_LS1_Total_Assets_Turnover = 42;
        final int index_LS2_Total_Assets_Turnover = 43;
        final int index_LS3_Total_Assets_Turnover = 44;

        ArrayList<ArrayList<String>> filteredDataSet = new ArrayList<>();
        //TODO: Remove ETF and Funds filtered results after testing;
        ArrayList<ArrayList<String>> ETF_Fund_DataSet = new ArrayList<>();

        //TODO: Parsing Array and filter out data of mutual fund and ETF;
        for (ArrayList<String> data : dataSet) {
            boolean IsStock = false;
            for (int i = index_LS1_GPM; i < index_LS3_Total_Assets_Turnover + 1; i++) {
                if (!data.get(i).equals("")) {
                    IsStock = true;
                    break;
                }
            }
            if (IsStock) {
                filteredDataSet.add(data);
            } else {
                ETF_Fund_DataSet.add(data);
            }
        }
        return filteredDataSet;
    }

    public static List<User> convertToUserList(ServiceResponse serviceResponse) {
        List<User> users = new ArrayList<>();

        Log.d(LOG_TAG, "Converting the response.");
        try {
            for (Data data : serviceResponse.data) {
                User user = new User();
                user.id = data.id;
                user.name = data.name;
                user.nickname = data.nickname;
                user.email = data.email;
                user.birthdate = data.birthdate;
                user.gender = data.gender;
                user.salary = data.salary;
                user.job = data.group.description;
                user.created_at = getDate(data.createdAt);
                user.updated_at = getDate(data.updatedAt);
                user.age = data.age;
                user.address = data.address.line1;
                user.zipcode = data.address.zipcode;
                user.mobile = data.address.mobile;
                user.fax = data.address.fax;

                // add
                users.add(user);
            }
        } catch (Exception e) {
            Log.d(LOG_TAG, "Converting the response process has been failed. ", e);
        }

        return users;
    }

    //TODO: Replace it to our private Parsing helpes
    private static Date getDate(String stringData) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        return format.parse(stringData);
    }

}
