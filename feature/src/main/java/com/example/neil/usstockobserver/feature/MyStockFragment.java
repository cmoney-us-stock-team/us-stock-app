package com.example.neil.usstockobserver.feature;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.neil.usstockobserver.feature.Leaderboard.InjectorUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyStockFragment extends Fragment {

    private ArrayList<StockItem> mStockItems = new ArrayList<>();
    private ArrayList<String> savedStockSymbolList = new ArrayList<>();
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SharedViewModel sharedViewModel;


    private final String PreferencesDataName = "MyStock";
    private final String TAG_STOCK_LIST = "stock_list";
    private RecyclerView mRecycleView;

    public MyStockFragment() {
        // Required empty public constructor
    }

    private void saveWatchedStockListToJson() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(PreferencesDataName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(savedStockSymbolList);
        System.out.println(json);
        editor.putString(TAG_STOCK_LIST, json);
        editor.apply();
        editor.commit();
    }

    private void LiveDataLoader() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(PreferencesDataName, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(TAG_STOCK_LIST, null );
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        savedStockSymbolList = gson.fromJson(json, type);
        if (savedStockSymbolList == null) {
            savedStockSymbolList = new ArrayList<>();
            savedStockSymbolList.add("AAPL");
            savedStockSymbolList.add("AMZN");
            savedStockSymbolList.add("MMM");
        }
        sharedViewModel.setSavedStockList(savedStockSymbolList);
//        SharedPreferences pref = getActivity().getSharedPreferences("MyStock1", Context.MODE_PRIVATE);
//        String name = pref.getString("name", "");
//        .setText(name);

    }

    private void insertItem(String stockName, String stockSymbol, String dailyClosedPrice, String sector){
        StockItem stockItem = new StockItem(stockName, stockSymbol, dailyClosedPrice, sector);
        mStockItems.add(stockItem);
        mAdapter.notifyItemInserted(mStockItems.size());
    }

    private void buildRecycleView(ItemTouchHelper itemTouchHelper){
        mRecycleView = getActivity().findViewById(R.id.stock_recycler);
        mRecycleView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        ((LinearLayoutManager) mLayoutManager).setOrientation(LinearLayoutManager.VERTICAL);
        mAdapter = new MyAdapter(mStockItems, sharedViewModel);

        mRecycleView.setLayoutManager(mLayoutManager);
        mRecycleView.setAdapter(mAdapter);
        itemTouchHelper.attachToRecyclerView(mRecycleView);
    }

    private ArrayList<StockItem> createTemperalStockData(){
        ArrayList<StockItem> stockItems = new ArrayList<>();
        ArrayList<String> stockSymbolList = new ArrayList<>();
        for ( int i = 0; i< 10; i++){
            StringBuilder stringBuilder = new StringBuilder("MMM");
            String stockSymbol =  stringBuilder.append(i).toString();
            String stockName = stockSymbol;
            Random random = new Random();
            int closedPrice = random.nextInt(25 + (i*i));
            String sector = "Tech.";
            StockItem stockItem = new StockItem(stockName, stockSymbol, String.valueOf(closedPrice), sector);
            stockItems.add(stockItem);
            stockSymbolList.add(stockSymbol);
        }
        this.savedStockSymbolList = stockSymbolList;
        return stockItems;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       //  Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_my_stock, container, false);
        //RecyclerView recycler_view = (RecyclerView) root.findViewById(R.id.stock_recycler);

      RecyclerView recycler_view = (RecyclerView) inflater.inflate(R.layout.fragment_my_stock, container, false);

//        final ArrayList<String> mStock = new ArrayList<>();
//        for (int i = 0; i < 50; i++) {
//            mStock.add("公司" + i);
//        }
//
//
//        final MyAdapter adapter = new MyAdapter(mStock);
//        recycler_view.setAdapter(adapter);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        recycler_view.setLayoutManager(layoutManager);




//        ItemTouchHelper.SimpleCallback mCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT,
//                ItemTouchHelper.START | ItemTouchHelper.END) {
//
//            //交換位置
//            @Override
//            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
//                int fromPosition = viewHolder.getAdapterPosition();
//                int toPosition = target.getAdapterPosition();
//                if (fromPosition < toPosition) {
//                    for (int i = fromPosition; i < toPosition; i++) {
//                        Collections.swap(mStock, i, i + 1);
//                    }
//                } else {
//                    for (int i = fromPosition; i > toPosition; i--) {
//                        Collections.swap(mStock, i, i - 1);
//                    }
//                }
//                adapter.notifyItemMoved(fromPosition, toPosition);
//                return true;
//            }
//
//            //刪除
//            @Override
//            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
//                int position = viewHolder.getAdapterPosition();
//                mStock.remove(position);
//                adapter.notifyItemRemoved(position);
//            }
//
//
//
//        };

        return recycler_view;
        //return inflater.inflate(R.layout.fragment_my_stock, container, false);
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecycleView = view.findViewById(R.id.stock_recycler);
        sharedViewModel = ViewModelProviders.of(getActivity(), InjectorUtils.getSharedViewModelFactory(getContext())).get(SharedViewModel.class);
        sharedViewModel.getSavedStockItemList().observe(getActivity(), new Observer<ArrayList<StockItem>>() {
            @Override
            public void onChanged(@Nullable ArrayList<StockItem> stockItems) {
                mStockItems = stockItems;
                mAdapter = new MyAdapter(mStockItems, sharedViewModel);
                mRecycleView.setAdapter(mAdapter);
            }
        });
//        mStockItems = createTemperalStockData();
//        LiveDataLoader();
        ItemTouchHelper.Callback mCallback = new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                //上下移動
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                //左右移動
                int swipeFlags =ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
                return makeMovementFlags(dragFlags, swipeFlags);
            }

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                int fromPosition = viewHolder.getAdapterPosition();
                int toPosition = target.getAdapterPosition();

//                Collections.swap(mStockItems, viewHolder.getAdapterPosition(), target.getAdapterPosition());
//                //通知Adapter它的Item发生了移动
//                mAdapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                StockItem stockItem = mStockItems.remove(fromPosition);
                mStockItems.add(toPosition, stockItem);
                mAdapter.notifyItemMoved(fromPosition, toPosition);
//                mAdapter.notifyItemChanged(fromPosition);
//                mAdapter.notifyItemChanged(toPosition);


//                if (fromPosition < toPosition) {
//                    for (int i = fromPosition; i < toPosition; i++) {
//                        //Collections.swap(mStock, i, i + 1);
//                        //TODO: Fix BUGGY Swap
//                        Collections.swap(mStockItems, i, i + 1);
//                    }
//                } else {
//                    for (int i = fromPosition; i > toPosition; i--) {
//                        //Collections.swap(mStock, i, i - 1);
//                        //TODO: Fix BUGGY Swap
//                        Collections.swap(mStockItems, i, i - 1);
//                    }
//                }
                //adapter.notifyItemMoved(fromPosition, toPosition);

                return true;
            }


            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
//                int position = viewHolder.getAdapterPosition();
//                mStock.remove(position);
//                adapter.notifyItemRemoved(position);

            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return false;
            }

            @Override
            public boolean isLongPressDragEnabled() { return true; }

        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(mCallback);
        buildRecycleView(itemTouchHelper);

//        ((Button)view.findViewById(R.id.test_btn)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onClick_test();
//            }
//        });
    }

//    public void onClick_test(){
//        TextView test = getView().findViewById(R.id.test_textview);
//        //test.setText((Self_Select_Home_Fragment)getParentFragment().);
//        //int poistion = ((Self_Select_Home_Fragment)Self_Select_StockFragment.this.getParentFragment()).currentPage;
//        ViewPager mPager = ((MyStockPagerFragment)MyStockFragment.this.getParentFragment()).getView().findViewById(R.id.pager);
//        int position = mPager.getCurrentItem();
//        Log.d("Pager", "My Child Position =" + position);
//        test.setText(Integer.toString(position));
//    }
}
