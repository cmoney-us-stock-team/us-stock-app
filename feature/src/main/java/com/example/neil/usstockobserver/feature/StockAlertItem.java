package com.example.neil.usstockobserver.feature;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.SparseArray;

import java.util.ArrayList;

public class StockAlertItem {

    @Nullable
    private String stockSymbol;
    @Nullable
    private String item1;
    @Nullable
    private String item2;
    @Nullable
    private String item3;
    @Nullable
    private String item4;
    @Nullable
    private String item5;
    @Nullable
    private String item6;
    @Nullable
    private String item7;
    @Nullable
    private ArrayList<Integer> HigherTargetPriceItems;
    @Nullable
    private ArrayList<Integer> LowerTargetPriceItems;

    private SparseArray<String> itemCategoryMap;


    public enum ItemCategory {
        代號(0),
        毛利率(1),
        淨利率(2),
        營業利益率(3),
        RoE(4),
        流動比(5),
        存貨周轉率(6),
        資產周轉率(7),
        價格高於(8),
        價格低於(9);
        private int index;

        ItemCategory(int value) {
            this.index = value;
        }

        public int getValue() {
            return index;
        }
    }

    String getStockSymbol() {
        return stockSymbol;
    }

    public String get_1_GrossProfitMargin() {
        return item1;
    }

    public String get_2_ProfitMargin() {
        return item2;
    }

    public String get_3_OperatingProfitMargin() {
        return item3;
    }

    public String get_4_ROE() {
        return item4;
    }

    public String get_5_CurrentRatio() {
        return item5;
    }

    public String get_6_InventoryTurnover() {
        return item6;
    }

    public String get_7_AssetTurnover() {
        return item7;
    }

    public ArrayList<Integer> getHigherTargetPriceItems() {
        return HigherTargetPriceItems;
    }

    public ArrayList<Integer> getLowerTargetPriceItems() {
        return LowerTargetPriceItems;
    }

    public void setStockSymbol(@Nullable String stockSymbol) {
        this.stockSymbol = stockSymbol;
    }

    public void set_1_GrossProfitMargin(@Nullable String item1) {
        this.item1 = item1;
    }

    public void set_2_ProfitMargin(@Nullable String item2) {
        this.item2 = item2;
    }

    public void set_3_OperatingProfitMargin(@Nullable String item3) {
        this.item3 = item3;
    }

    public void set_4_ROE(@Nullable String item4) {
        this.item4 = item4;
    }

    public void set_5_CurrentRatio(@Nullable String item5) {
        this.item5 = item5;
    }

    public void gst_6_InventoryTurnover(@Nullable String item6) {
        this.item6 = item6;
    }

    public void set_7_AssetTurnover(@Nullable String item7) {
        this.item7 = item7;
    }

    public void setHigherTargetPriceItems(ArrayList<Integer> item8) {
        this.HigherTargetPriceItems = item8;
    }

    public void setLowerTargetPriceItems(ArrayList<Integer> lowerTargetPriceItems) {
        this.LowerTargetPriceItems = lowerTargetPriceItems;
    }


    public String getCategoryName(ItemCategory item, Context context) {
        if (itemCategoryMap == null) {
            itemCategoryMap = parseStringArray(context, R.array.alertItemCategoryMap);
        }
        return itemCategoryMap.get(item.getValue());
    }


    private ArrayList<String> getItemName(Context context) {
        ArrayList<String> itemNames = new ArrayList<>();
        String[] items = context.getResources().getStringArray(R.array.alertItemCategoryName);
        for (int i = 0; i < items.length; i++) {
            itemNames.add(items[i]);
        }
        return itemNames;
    }

    public SparseArray<String> parseStringArray(Context context, int stringArrayResourceId) {
        String[] stringArray = context.getResources().getStringArray(stringArrayResourceId);
        SparseArray<String> outputArray = new SparseArray<String>(stringArray.length);
        for (String entry : stringArray) {
            String[] splitResult = entry.split("\\|", 2);
            outputArray.put(Integer.valueOf(splitResult[0]), splitResult[1]);
        }
        return outputArray;
    }

}
