package com.example.neil.usstockobserver.feature;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.example.neil.usstockobserver.feature.Leaderboard.InjectorUtils;

public class SingleStockActivity extends AppCompatActivity {

    SharedViewModel mSharedViewModel;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_stock);
        mSharedViewModel = ViewModelProviders.of(this, InjectorUtils.getSharedViewModelFactory(getBaseContext())).get(SharedViewModel.class);
        toolbar = (Toolbar) findViewById(R.id.toolbar_single);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        String stockSymbol = mSharedViewModel.getSelectedCurrentStock().getValue();
        if (stockSymbol == null) {
            stockSymbol = "";
        }
        String title = new StringBuilder().append("個股").append(" - ").append(stockSymbol).toString();
        toolbar.setTitle(title);

        //將sectionPagerAdapter指派給ViewPager  一個新的類別
        SectionsPagerAdapter pagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        ViewPager stock_view = (ViewPager) findViewById(R.id.single_stock);
        stock_view.setAdapter(pagerAdapter);

        //將ViewPager指派給Tablayot
        TabLayout tabLayout2 = (TabLayout) findViewById(R.id.tabs2);
        tabLayout2.setupWithViewPager(stock_view);

    }


    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        //建構子
        public SectionsPagerAdapter(FragmentManager fm){
            super(fm);
        }
        //回傳view Pager的頁數
        @Override
        public int getCount(){
            return 2;
        }

        //指定對應的fragment頁面
        //@Override
        public Fragment getItem(int position){
            //Page=position;
            Log.d("single_stock", "Position =" + position);
            switch (position){
                case 0:
                    return new SingleStockFragment();
                case 1:
                    return new NotifySingleFragment();
            }
            return null;
        }

        //將文字加到標籤裡
        public CharSequence getPageTitle(int position){
            switch (position){
                case 0:
                    return getResources().getText(R.string.single_stock);
                case 1:
                    return getResources().getText(R.string.notification);
            }
            return null;
        }
    }

}
