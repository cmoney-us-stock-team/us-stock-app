package com.example.neil.usstockobserver.feature;

public class StockItem {
    private String stockName;
    private String stockSymbol;
    private String dailyClosedPrice;
    private String sector;

    public StockItem(String stockName, String stockSymbol, String dailyClosedPrice, String sector) {
        this.stockName = stockName;
        this.stockSymbol = stockSymbol;
        this.dailyClosedPrice = dailyClosedPrice;
        this.sector = sector;
    }

    public String getStockName() {
        return stockName;
    }

    public String getStockSymbol() {
        return stockSymbol;
    }

    public String getDailyClosedPrice() {
        return dailyClosedPrice;
    }

    public String getSector() {
        return sector;
    }


}
