package com.example.neil.usstockobserver.feature;



import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.renderscript.Sampler;
import android.support.annotation.CheckResult;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.evrencoskun.tableview.ITableView;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.holder.ColumnHeaderViewHolder;

import java.util.zip.CheckedOutputStream;

import androidx.annotation.Nullable;

public class SetPrefActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {


//    public SharedPreferences settings;
//    private ITableView m_iTableView;
//    private ColumnHeaderViewHolder m_iViewHolder;
//    private Context mContext;
//    private int mXPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        prefs.registerOnSharedPreferenceChangeListener(this);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        MyPrefFragment myPreference = new MyPrefFragment();

//        Preference preference = new Preference(myPreference.getPreferenceScreen().getContext());
//        preference.setIconSpaceReserved(false);

        transaction.replace(android.R.id.content,  myPreference);
        transaction.commit();
    }

//    public SettingControler(ColumnHeaderViewHolder p_iViewHolder, ITableView
//            p_jTableView) {
//        super(p_iViewHolder.itemView.getContext(), p_iViewHolder.itemView);
//        this.m_iViewHolder = p_iViewHolder;
//        this.m_iTableView = p_jTableView;
//        this.mContext = p_iViewHolder.itemView.getContext();
//        this.mXPosition = m_iViewHolder.getAdapterPosition();
//
//        // find the view holder
//        m_iViewHolder = (ColumnHeaderViewHolder) m_iTableView.getColumnHeaderRecyclerView()
//                .findViewHolderForAdapterPosition(mXPosition);
//    }

//    SharedPreferences setting_pref = getSharedPreferences("setting", MODE_PRIVATE);
//    SharedPreferences.Editor editor = setting_pref.edit();
//    Boolean control=setting_pref.getBoolean("duration1",false);

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        Boolean set_pref = sharedPreferences.getBoolean(key,true);
        SharedPreferences spref=getSharedPreferences("set",MODE_PRIVATE);
        SharedPreferences.Editor editor=spref.edit();

        if(set_pref==true){
            editor.putString(key,"");
            editor.putBoolean(key,true);
            editor.commit();

//            Toast.makeText(this,
//                    "key: " + key+"value"+set_pref ,
//                    Toast.LENGTH_LONG).show();
        }

    }

        //Boolean duration1 = setting_pref.getBoolean("duration1",true);
//        if(key.equals("duration1")){
//            editor.putBoolean("duration1",);
//            editor.commit();
//        }
//        if(set_pref.booleanValue()==true){
//            editor.putString("Key",key);
//            editor.commit();
//        }


       // String set_key = sharedPreferences.getString("KEY_STRING", null);
//        SharedPreferences spref = getPreferences(MODE_PRIVATE);
//        SharedPreferences.Editor editor = spref.edit();
//        editor.putBoolean("KEY_BOOL", true);
//        editor.putString("KEY_STRING", key);
//        editor.commit();

//        Toast.makeText(this,
//                "key: " + key+"value"+set_pref ,
//                Toast.LENGTH_LONG).show();
//        if(key.equals("duration1")) {
//            m_iTableView.hideColumn(5);
//            m_iTableView.hideColumn(7);
//
//        }
    }


