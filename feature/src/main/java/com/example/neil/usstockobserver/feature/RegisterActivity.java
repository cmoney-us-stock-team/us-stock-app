package com.example.neil.usstockobserver.feature;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import butterknife.BindView;
//import butterknife.ButterKnife;

/**
 * A login screen that offers login via email/password.
 */
public class RegisterActivity extends AppCompatActivity {

    private final String loginServiceUrl = "https://www.cmoney.tw/MobileService/ashx/";
    private static final int AppID = 2;
    private static final int Device_Android = 2;
    private static final boolean Need_Broadcast = false;
    private static final String Broadcast_Token = "";
    private static final String Action_DefaultLogin = "getloginguid";
    private static final String Action_Forgot_Password = "forgetpassword";
    private static final String Action_Register_Account = "registeraccount";
    private static final String Action_Get_MemberProfile = "getmemberprofile";
    private static final int Login_Response_Successful = 1;
    private static final int Login_Response_AuthedFail = 2;
    private static final int Register_Response_Successful = 1;
    private static final int Register_Response_Duplicate = 4;
    private static final int Register_Response_Duplicate2 = 6;
    private static final int Register_Response_Duplicate3 = 11;
    private static final int Login_Response_ServerNoResponse = 9002;

    private static final String TAG = "SignupActivity";

    ProgressDialog mProgressDialog;

    EditText _emailText;
    EditText _passwordText;
    EditText _reEnterPasswordText;
    Button _signupButton;
    TextView _loginLink;
    private View mSignUpFormView;
    private View mProgressView;

    /*
//    @BindView(R.id.input_name) EditText _nameText;
//    @BindView(R.id.input_address) EditText _addressText;
    @BindView(R2.id.input_email) EditText _emailText;
//    @BindView(R.id.input_mobile) EditText _mobileText;
    @BindView(R2.id.input_password) EditText _passwordText;
    @BindView(R2.id.input_confirm_password) EditText _reEnterPasswordText;
    @BindView(R2.id.btn_signup) Button _signupButton;
    @BindView(R2.id.link_login) TextView _loginLink;
*/
    private void bindView(){
        _emailText = findViewById(R.id.input_email);
        _passwordText = findViewById(R.id.input_password);
        _reEnterPasswordText = findViewById(R.id.input_confirm_password);
        _signupButton = findViewById(R.id.btn_signup);
        _loginLink = findViewById(R.id.link_login);
        mSignUpFormView = findViewById(R.id.signUp_form);
        mProgressView = findViewById(R.id.signUp_progress);
        mProgressDialog = new ProgressDialog(RegisterActivity.this,
                com.example.neil.usstockobserver.R.style.AppTheme_Dark_Dialog);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getString(R.string.prompt_success_signup));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        bindView();
        //ButterKnife.bind(this);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

    }

    public void signup() {
        //TODO: Add Api Logics
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        _signupButton.setEnabled(false);


//        String name = _nameText.getText().toString();
//        String address = _addressText.getText().toString();
        String email = _emailText.getText().toString();
//        String mobile = _mobileText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        // TODO: Implement your own signup logic here.
        showProgress(true);
        signupApiRequest(loginServiceUrl, email, MD5(password));

//        new android.os.Handler().postDelayed(
//                new Runnable() {
//                    public void run() {
//                        // On complete call either onSignupSuccess or onSignupFailed
//                        // depending on success
//                        onSignupSuccess();
//                        // onSignupFailed();
//                        progressDialog.dismiss();
//                    }
//                }, 3000);
    }


    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

//        String name = _nameText.getText().toString();
//        String address = _addressText.getText().toString();
        String email = _emailText.getText().toString();
//        String mobile = _mobileText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

//        if (name.isEmpty() || name.length() < 3) {
//            _nameText.setError("at least 3 characters");
//            valid = false;
//        } else {
//            _nameText.setError(null);
//        }
//
//        if (address.isEmpty()) {
//            _addressText.setError("Enter Valid Address");
//            valid = false;
//        } else {
//            _addressText.setError(null);
//        }


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

//        if (mobile.isEmpty() || mobile.length()!=10) {
//            _mobileText.setError("Enter Valid Mobile Number");
//            valid = false;
//        } else {
//            _mobileText.setError(null);
//        }

        if (password.isEmpty() || password.length() < 6 || password.length() > 10) {
            //_passwordText.setError("between 6 and 10 alphanumeric characters");
            _passwordText.setError(getResources().getString(R.string.register_password_error));
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 6 || reEnterPassword.length() > 10 || !(reEnterPassword.equals(password))) {
            _reEnterPasswordText.setError("Password Do not match");
            valid = false;
        } else {
            _reEnterPasswordText.setError(null);
        }

        return valid;
    }

    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes("UTF-8"));
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        } catch (UnsupportedEncodingException ex) {
        }
        return null;
    }

    private void signupApiRequest(String serviceUrl, String account, String hashedPassword) {

        //TODO: Refactor it  into Singleton mode
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<LoginResponse> loginResponseCall = jsonPlaceHolderApi.Register(
                RegisterActivity.Action_Register_Account, account, hashedPassword, RegisterActivity.Device_Android);

        //TODO: REFACTOR
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Intent intent = null;
                LoginResponse loginResponse = response.body();
                showProgress(false);

                if (!response.isSuccessful()) {
                    Log.d("Login", ("Code: " + response.code()));
                    Toast.makeText(getApplicationContext(), "Error Response Code: " + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }

                switch (loginResponse.getResponseCode()) {
                    case Register_Response_Successful:
                        Toast.makeText(getApplicationContext(), "Register successfully", Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(), LoginActivity.class);
                        break;
                    case Register_Response_Duplicate:
                    case Register_Response_Duplicate2:
                    case Register_Response_Duplicate3:
                        //TODO: REFACTOR THIS LOGIC
                        _emailText.setError(getString(R.string.error_account_exist));
                        _emailText.requestFocus();
                        break;
                    default:
                        Toast.makeText(getApplicationContext(), "Connection problem occurred,\nplease try again later", Toast.LENGTH_SHORT);
                        break;
                }
                _signupButton.setEnabled(true);
                Toast.makeText(getApplicationContext(), "HTTP Response Code: " + response.code() + "\n" + loginResponse.getAuthToken(), Toast.LENGTH_LONG).show();
                if (intent != null) {
                    //TODO: Change Pop-up dialog with a tick mark.
                    mProgressDialog.show();
                    //TODO: Add animation delay
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d("Login", ("Failed: " + t.toString()));
                Toast.makeText(getApplicationContext(), "Uable to connect to the server", Toast.LENGTH_SHORT).show();
                showProgress(false);
                onSignupFailed();
            }
        });
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSignUpFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

