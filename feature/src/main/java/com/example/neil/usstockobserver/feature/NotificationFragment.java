package com.example.neil.usstockobserver.feature;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import static com.example.neil.usstockobserver.feature.NotifyChannel.Notify;


public class NotificationFragment extends Fragment {

public boolean notification_control=false;
    public NotificationFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setHasOptionsMenu(true); //讓menu顯示出來
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        TextView textView = view.findViewById(R.id.tv_title);
        textView.setText("通知總攬");
        //toolbar.inflateMenu(R.menu.toolbar_menu);

        //隱藏toolbar項目
//        Menu menu = toolbar.getMenu();
//        MenuItem item = menu.findItem(R.id.action_add_setting);
//        item.setVisible(false);



    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.toolbar_menu, menu);
//
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_notification, container, false);

        Button test=(Button)view.findViewById(R.id.test);
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notification_control=true;
                showNotification();
            }
        });


        return view;
    }

    private PendingIntent pendingIntentForNotification() {
        //Create the intent you want to show when the notification is clicked
        Intent intent = new Intent(getActivity(), MainFrameActivity.class);
        //intent.putExtra(MainFrameActivity.EXTRA_FRAGMENT_TO_LAUNCH, MainFrameActivity.TAG_NOTIFICATION_FRAGMENT);
        //intent.putExtra(MainFrameActivity.fragment1, MainFrameActivity.fragment3);

        //This will hold the intent you've created until the notification is tapped.
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 1, intent, 0);
        return pendingIntent;
    }

    private String getExampleNotificationString() {
        return "AAPL價格已到達200";
    }

    private void showNotification() {
        //Use the NotificationCompat compatibility library in order to get gingerbread support.
        Notification notification = new NotificationCompat.Builder(getActivity(),Notify)
                //Title of the notification
                .setContentTitle(getString(R.string.notify))
                //Content of the notification once opened
                .setContentText(getExampleNotificationString())
                //Icon that shows up in the notification area
                .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                //Set the intent
                .setContentIntent(pendingIntentForNotification())
                //Build the notification with all the stuff you've just set.
                .build();
        //Add the auto-cancel flag to make it dismiss when clicked on
        //This is a bitmask value so you have to pipe-equals it.
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        //Grab the NotificationManager and post the notification
        NotificationManager notificationManager = (NotificationManager)
                getActivity().getSystemService(getActivity().NOTIFICATION_SERVICE);

        //Set a tag so that the same notification doesn't get reposted over and over again and
        //you can grab it again later if you need to.
        notificationManager.notify(1, notification);
    }


}
