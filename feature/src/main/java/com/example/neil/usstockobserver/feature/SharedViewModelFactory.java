package com.example.neil.usstockobserver.feature;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.neil.usstockobserver.feature.Leaderboard.Data.network.UserRepository;

public class SharedViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final UserRepository userRepository;

    public SharedViewModelFactory(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        //noinspection unchecked
        return (T) new SharedViewModel(userRepository);
    }
}
