//package com.example.neil.usstockobserver.feature;
//
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.design.widget.FloatingActionButton;
//import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentPagerAdapter;
//import android.support.v4.view.MenuItemCompat;
//import android.support.v4.view.ViewPager;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.ShareActionProvider;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Toast;
//
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class SingleStockPagerFragment extends Fragment {
//
//    private ShareActionProvider shareActionProvider;
//    public int Page;
//    public SingleStockPagerFragment() {
//        // Required empty public constructor
//    }
//
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        setHasOptionsMenu(true); //讓menu顯示出來
//        return inflater.inflate(R.layout.fragment_single_stock_pager, container, false);
//    }
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//
//        Toolbar toolbar = view.findViewById(R.id.toolbar);
//        toolbar.inflateMenu(R.menu.toolbar_menu);
//
//        //隱藏toolbar選項
//        Menu menu = toolbar.getMenu();
//        MenuItem item = menu.findItem(R.id.stock_search);
//        item.setVisible(false);
//        //((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
//        //((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
//        //((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//        //將sectionPagerAdapter指派給ViewPager  一個新的類別
//        SingleStockPagerFragment.SectionsPagerAdapter pagerAdapter = new SingleStockPagerFragment.SectionsPagerAdapter(getChildFragmentManager());
//        ViewPager stock_view = (ViewPager) getView().findViewById(R.id.single_stock);
//        stock_view.setAdapter(pagerAdapter);
//
//        //將ViewPager指派給Tablayot
//        TabLayout tabLayout2 = (TabLayout) getView().findViewById(R.id.tabs2);
//        tabLayout2.setupWithViewPager(stock_view);
//    }
//
//
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.toolbar_menu, menu);
//
//
//
//
//
////        menu.findItem(R.id.action_add_setting).setVisible(false);
////        MenuItem setting = menu.findItem(R.id.action_add_setting);
////        setting.setVisible(false);
//    }
//
//    private class SectionsPagerAdapter extends FragmentPagerAdapter {
//
//        //建構子
//        public SectionsPagerAdapter(FragmentManager fm){
//            super(fm);
//        }
//        //回傳view Pager的頁數
//        @Override
//        public int getCount(){
//            return 2;
//        }
//
//        //指定對應的fragment頁面
//        //@Override
//        public Fragment getItem(int position){
//          Page=position;
//          Log.d("single_stock", "Position =" + position);
//            switch (position){
//                case 0:
//                    return new SingleStockFragment();
//                case 1:
//                    return new NotifySingleFragment();
//            }
//            return null;
//        }
//
//        //將文字加到標籤裡
//        public CharSequence getPageTitle(int position){
//            switch (position){
//                case 0:
//                    return getResources().getText(R.string.single_stock);
//                case 1:
//                    return getResources().getText(R.string.notification);
//            }
//            return null;
//        }
//    }
//}


