package com.example.neil.usstockobserver.feature.Leaderboard.UI.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.neil.usstockobserver.feature.Leaderboard.Data.User;
import com.example.neil.usstockobserver.feature.Leaderboard.Data.network.UserRepository;
import com.example.neil.usstockobserver.feature.Leaderboard.model.ServiceRequest;

import java.util.ArrayList;
import java.util.List;

public class MainViewModel extends ViewModel {

    private final UserRepository mRepository;
    private final LiveData<ArrayList<ArrayList<String>>> mWholeStockDataSet;
    private final LiveData<List<User>> mUserData;

    public MainViewModel(UserRepository mRepository) {
        this.mRepository = mRepository;
        this.mUserData = mRepository.getUserList();
        this.mWholeStockDataSet = mRepository.getWholeStockDataSet();
    }

    public LiveData<List<User>> getUserList() {
        return mUserData;
    }

    public LiveData<ArrayList<ArrayList<String>>> getWholeStockDataSet() {
        return mWholeStockDataSet;
    }

    public void postWholeStockServiceRequest() {
        mRepository.postWholeStockServiceRequest();
    }

    public void postRequest(ServiceRequest serviceRequest) {
        mRepository.postServiceRequest(serviceRequest);
    }
}
