package com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.model;

import com.evrencoskun.tableview.sort.ISortableModel;

/**
 * Created by evrencoskun on 27.11.2017.
 */

public class CellModel implements ISortableModel {
    private String mId;
    private Object mData;
    private String mStockSymbol;

    public void setDisplayAsEmpty(boolean empty) {
        displayAsEmpty = empty;
    }

    public boolean IsEmpty() {
        return displayAsEmpty;
    }

    private boolean displayAsEmpty = false;

    public CellModel(String pId, Object mData) {
        this.mId = pId;
        this.mData = mData;
    }

    public CellModel(String pId, Object mData, String stockSymbol) {
        this.mId = pId;
        this.mData = mData;
        this.mStockSymbol = stockSymbol;
    }


    public Object getData() {
        return mData;
    }

    public String getStockSymbol() {
        if (mStockSymbol == null) {
            return "";
        }
        return mStockSymbol;
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public Object getContent() {
        return mData;
    }

}
