package com.example.neil.usstockobserver.feature;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.neil.usstockobserver.feature.Leaderboard.Data.network.UserRepository;

import java.util.ArrayList;

public class SharedViewModel extends ViewModel {
    private final UserRepository mRepository;
    private final MutableLiveData<String> currentViewStock;
    private final MutableLiveData<ArrayList<String>> mSavedStockList;
    private final MutableLiveData<ArrayList<StockAlertItem>> stockAltertItems;
    private final LiveData<ArrayList<ArrayList<String>>> mWholeStockDataSet;
    private final LiveData<ArrayList<StockItem>> mSavedStockItemList;

    public SharedViewModel(UserRepository mRepository) {
        this.mRepository = mRepository;
        this.currentViewStock = mRepository.getCurrentViewStock();
        this.mWholeStockDataSet = mRepository.getWholeStockDataSet();
        this.stockAltertItems = mRepository.getStockAlertItems();
        this.mSavedStockList = mRepository.getSavedStockList();
        this.mSavedStockItemList = mRepository.getWholeWatchedItems();
    }

    public void selectCurrentStock(String stockSymbol){
        currentViewStock.setValue(stockSymbol);
    }
    public LiveData<String> getSelectedCurrentStock(){
        return currentViewStock;
    }

    public LiveData<ArrayList<ArrayList<String>>> getWholeStockDataSet() {
        return mWholeStockDataSet;
    }

    public LiveData<ArrayList<String>> getSavedStockList() {
        return mSavedStockList;
    }

    public void setSavedStockList(ArrayList<String> savedStockList) {
        if (savedStockList != null) {
            mSavedStockList.setValue(savedStockList);
        }
    }

    public LiveData<ArrayList<StockItem>> getSavedStockItemList() {
        return mSavedStockItemList;
    }


}
