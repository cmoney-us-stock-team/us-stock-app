package com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.neil.usstockobserver.feature.Leaderboard.Data.User;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.model.CellModel;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.model.ColumnHeaderModel;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.model.RowHeaderModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

/**
 * Created by evrencoskun on 4.02.2018.
 */

public class MyTableViewModel {

    public enum DATA_PERIOD {
        ONE_QUARTER,
        THREE_QUARTERS,
        ONE_YEAR,
    }

    // View Types
    public static final int GENDER_TYPE = 1;
    public static final int MONEY_TYPE = 2;

    private List<ColumnHeaderModel> mColumnHeaderModelList;
    private List<RowHeaderModel> mRowHeaderModelList;
    private List<List<CellModel>> mCellModelList;

    public int getCellItemViewType(int column) {

        switch (column) {
            case 5:
                // 5. column header is gender.
                return GENDER_TYPE;
            case 8:
                // 8. column header is Salary.
                return MONEY_TYPE;
            default:
                return 0;
        }
    }

     /*
       - Each of Column Header -
            "Id"
            "Name"
            "Nickname"
            "Email"
            "Birthday"
            "Gender"
            "Age"
            "Job"
            "Salary"
            "CreatedAt"
            "UpdatedAt"
            "Address"
            "Zip Code"
            "Phone"
            "Fax"
     */

//    public int getColumnTextAlign(int column) {
//        switch (column) {
//            // Id
//            case 0:
//                return Gravity.CENTER;
//            // Name
//            case 1:
//                return Gravity.LEFT;
//            // Nickname
//            case 2:
//                return Gravity.LEFT;
//            // Email
//            case 3:
//                return Gravity.LEFT;
//            // BirthDay
//            case 4:
//                return Gravity.CENTER;
//            // Gender (Sex)
//            case 5:
//                return Gravity.CENTER;
//            // Age
//            case 6:
//                return Gravity.CENTER;
//            // Job
//            case 7:
//                return Gravity.LEFT;
//            // Salary
//            case 8:
//                return Gravity.CENTER;
//            // CreatedAt
//            case 9:
//                return Gravity.CENTER;
//            // UpdatedAt
//            case 10:
//                return Gravity.CENTER;
//            // Address
//            case 11:
//                return Gravity.LEFT;
//            // Zip Code
//            case 12:
//                return Gravity.RIGHT;
//            // Phone
//            case 13:
//                return Gravity.RIGHT;
//            // Fax
//            case 14:
//                return Gravity.RIGHT;
//            default:
//                return Gravity.CENTER;
//        }
//
//    }

    private List<ColumnHeaderModel> createColumnHeaderModelList() {
        List<ColumnHeaderModel> list = new ArrayList<>();
        // Create Column Headers
        list.add(new ColumnHeaderModel("名稱")); //1
        list.add(new ColumnHeaderModel("代碼")); //0
        list.add(new ColumnHeaderModel("昨收")); //2
        list.add(new ColumnHeaderModel("漲跌幅(Q%)")); //7
        list.add(new ColumnHeaderModel("漲跌幅(Y%)")); //15
        list.add(new ColumnHeaderModel("毛利率(Q%)")); //24
        list.add(new ColumnHeaderModel("毛利率(Y%)")); //26
        list.add(new ColumnHeaderModel("稅後純益率(Q%)")); //27
        list.add(new ColumnHeaderModel("稅後純益率(Y%)")); //29
        list.add(new ColumnHeaderModel("營業利益率(Q%)")); //30
        list.add(new ColumnHeaderModel("營業利益率(Y%)")); //32
        list.add(new ColumnHeaderModel("稅後純益率(Q%)")); //33
        list.add(new ColumnHeaderModel("稅後純益率(Y%)")); //35
        list.add(new ColumnHeaderModel("流動比(Q%)")); //36
        list.add(new ColumnHeaderModel("流動比(Y%)")); //38
        list.add(new ColumnHeaderModel("存貨週轉率(Q次)")); //39
        list.add(new ColumnHeaderModel("存貨週轉率(Y次)")); //41
        list.add(new ColumnHeaderModel("總資產週轉率(Q次)")); //42
        list.add(new ColumnHeaderModel("總資產週轉率(Y次)")); //44
        list.add(new ColumnHeaderModel("產業分類")); //20

        return list;
    }

    //TODO: Refactor WholeStock DataSet Parsing logics
    private List<List<CellModel>> createCellModelList(ArrayList<ArrayList<String>> wholeStockDataSet) {
        List<List<CellModel>> lists = new ArrayList<>();
        ArrayList<Integer> validMappingIndex = getValidColumnIndexListHelper();
        ArrayList<Integer> stringIndex = getStringColumnIndexListHelper();
        // Creating cell model list from User list for Cell Items
        // In this example, User list is populated from web service

        for (ArrayList<String> stockData : wholeStockDataSet) {

            List<CellModel> list = new ArrayList<>();
            String stockSymbol = stockData.get(0);
            for (int i = 0; i < validMappingIndex.size(); i++) {
                int dataIndex = validMappingIndex.get(i);
                StringBuilder stringBuilder = new StringBuilder(i).
                        append("-").
                        append(i);
                String pid = stringBuilder.toString();
                String celldata = stockData.get(dataIndex);
                if (stringIndex.contains(dataIndex)) {
                    list.add(new CellModel(pid, celldata, stockSymbol));
                }else {
                    CellModel cellModel;
                    try{
                        float number = Float.parseFloat(celldata);
                        cellModel = new CellModel(pid, number, stockSymbol);
                        list.add(cellModel);
                    }catch (NumberFormatException e){
                        float number = Float.NaN;
                        cellModel = new CellModel(pid, number, stockSymbol);
                        cellModel.setDisplayAsEmpty(true);
                        list.add(cellModel);
                    }
                }
            }
            lists.add(list);
        }
        return lists;
    }

    private List<List<CellModel>> createCellModelList(List<User> userList) {
        List<List<CellModel>> lists = new ArrayList<>();

        // Creating cell model list from User list for Cell Items
        // In this example, User list is populated from web service

        for (int i = 0; i < userList.size(); i++) {
            User user = userList.get(i);

            List<CellModel> list = new ArrayList<>();

            // The order should be same with column header list;
            list.add(new CellModel("1-" + i, user.id));          // "Id"
            list.add(new CellModel("2-" + i, user.name));        // "Name"
            list.add(new CellModel("3-" + i, user.nickname));    // "Nickname"
            list.add(new CellModel("4-" + i, user.email));       // "Email"
            list.add(new CellModel("5-" + i, user.birthdate));   // "BirthDay"
            list.add(new CellModel("6-" + i, user.gender));      // "Gender"
            list.add(new CellModel("7-" + i, user.age));         // "Age"
            list.add(new CellModel("8-" + i, user.job));         // "Job"
            list.add(new CellModel("9-" + i, user.salary));      // "Salary"
            list.add(new CellModel("10-" + i, user.created_at)); // "CreatedAt"
            list.add(new CellModel("11-" + i, user.updated_at)); // "UpdatedAt"
            list.add(new CellModel("12-" + i, user.address));    // "Address"
            list.add(new CellModel("13-" + i, user.zipcode));    // "Zip Code"
            list.add(new CellModel("14-" + i, user.mobile));     // "Phone"
            list.add(new CellModel("15-" + i, user.name));        // "Fax"
            list.add(new CellModel("16-" + i, user.name));        // "Fax"
            list.add(new CellModel("17-" + i, user.name));        // "Fax"
            list.add(new CellModel("18-" + i, user.name));        // "Fax"
            list.add(new CellModel("19-" + i, user.name));        // "Fax"
            list.add(new CellModel("20-" + i, user.name));        // "Fax"

            // Add
            lists.add(list);
        }

        return lists;
    }

    private List<RowHeaderModel> createRowHeaderList(int size, ArrayList<ArrayList<String>> wholeStockDataSet) {
        List<RowHeaderModel> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            String stockSymbol = wholeStockDataSet.get(i).get(0);
            // In this example, Row headers just shows the index of the TableView List.
//            list.add(new RowHeaderModel(String.valueOf(i + 1)));
            list.add(new RowHeaderModel(stockSymbol, stockSymbol));
        }
        return list;
    }

    private List<RowHeaderModel> createRowHeaderList(int size) {
        List<RowHeaderModel> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            // In this example, Row headers just shows the index of the TableView List.
            list.add(new RowHeaderModel(String.valueOf(i + 1)));
        }
        return list;
    }


    public List<ColumnHeaderModel> getColumHeaderModeList() {
        return mColumnHeaderModelList;
    }

    public List<RowHeaderModel> getRowHeaderModelList() {
        return mRowHeaderModelList;
    }

    public List<List<CellModel>> getCellModelList() {
        return mCellModelList;
    }


    public void generateWholeStockListForTableView(ArrayList<ArrayList<String>> wholeStockDataSet) {
        mColumnHeaderModelList = createColumnHeaderModelList();
        mCellModelList = createCellModelList(wholeStockDataSet);
        mRowHeaderModelList = createRowHeaderList(wholeStockDataSet.size(), wholeStockDataSet);
    }

    public void generateListForTableView(List<User> users) {
        mColumnHeaderModelList = createColumnHeaderModelList();
        mCellModelList = createCellModelList(users);
        mRowHeaderModelList = createRowHeaderList(users.size());
    }

    private ArrayList<Integer> getValidColumnIndexListHelper() {
        ArrayList<Integer> list = new ArrayList<>();
        //TODO: Review this index mapping
        int[] validIndexes = {1, 0, 2, 7, 15, 24, 26, 27, 29, 30, 32, 33, 35, 36, 38, 39, 41, 42, 44, 20};
        for (int i = 0; i < validIndexes.length; i++) {
            list.add(validIndexes[i]);
        }
        return list;
    }

    private ArrayList<Integer> getStringColumnIndexListHelper() {
        ArrayList<Integer> list = new ArrayList<>();
        //TODO: Review this index mapping
        int[] validIndexes = {1, 0, 20};
        for (int i = 0; i < validIndexes.length; i++) {
            list.add(validIndexes[i]);
        }
        return list;
    }
}



