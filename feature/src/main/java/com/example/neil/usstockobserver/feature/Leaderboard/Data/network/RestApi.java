package com.example.neil.usstockobserver.feature.Leaderboard.Data.network;

import com.example.neil.usstockobserver.feature.Leaderboard.model.ServiceResponse;
import com.example.neil.usstockobserver.feature.SingleStockDataResponse;
import com.example.neil.usstockobserver.feature.WholeStockResponse;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestApi {

    @GET("users")
    Observable<ServiceResponse> getUser(@Query("per_page") int size, @Query("current_page") int page);

    @GET("GetDtnoData.ashx")
    Observable<SingleStockDataResponse> getSingleStock(
            @Query("Action")String Action,
            @Query("DtNo") int DtNo,
            //@Query(value="ParamStr", encoded=true) String ParamStr,
            @Query("ParamStr") String ParamStr,
            @Query("FilterNo") int FilterNo,
            @Query("KeyMap") String KeyMap,
            @Query("AssignSPID") String AssignSPID
    );


    @GET("GetDtnoData.ashx")
    Observable<WholeStockResponse>getWholeStock(
            @Query("Action")String Action,
            @Query("DtNo") int DtNo,
            //@Query(value="ParamStr", encoded=true) String ParamStr,
            @Query("ParamStr") String ParamStr,
            @Query("FilterNo") int FilterNo,
            @Query("KeyMap") String KeyMap,
            @Query("AssignSPID") String AssignSPID
    );
}
