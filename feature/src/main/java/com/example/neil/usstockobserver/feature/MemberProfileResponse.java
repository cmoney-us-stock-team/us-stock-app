package com.example.neil.usstockobserver.feature;

public class MemberProfileResponse {

    private String ImagePath;
    private String NickName;
    private String Email;
    private String Email2;
    private int ResponseCode;
    private String ResponseMsg;

    public String getImagePath() {
        return ImagePath;
    }

    public String getNickName() {
        return NickName;
    }

    public String getEmail() {
        return Email;
    }

    public String getEmail2() {
        return Email2;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public String getResponseMsg() {
        return ResponseMsg;
    }
}
