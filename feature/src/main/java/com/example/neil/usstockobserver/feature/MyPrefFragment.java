package com.example.neil.usstockobserver.feature;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.support.v14.preference.SwitchPreference;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.resources.TextAppearance;
import android.support.design.resources.TextAppearanceConfig;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.time.format.TextStyle;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPrefFragment extends PreferenceFragmentCompat {

    public static final String PREFERENCES_NAME = "myPreference";
    private SwitchPreference sw1,sw2,sw3,sw4,sw5,sw6,sw7,sw8;
    private CheckBoxPreference d1,d2;

    public MyPrefFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preference);
        sw1=(SwitchPreference)findPreference("sw1");
        sw2=(SwitchPreference)findPreference("sw2");
        sw3=(SwitchPreference)findPreference("sw3");
        sw4=(SwitchPreference)findPreference("sw4");
        sw5=(SwitchPreference)findPreference("sw5");
        sw6=(SwitchPreference)findPreference("sw6");
        sw7=(SwitchPreference)findPreference("sw7");
        sw8=(SwitchPreference)findPreference("sw8");
        d1=(CheckBoxPreference)findPreference("duration1");
        d2=(CheckBoxPreference)findPreference("duration2");

        //sw1.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
        //    @Override
        //    public boolean onPreferenceChange(Preference preference, Object o) {
        //        if(sw1.isChecked())
        //        {Toast.makeText(getActivity(),"sw1 Unchecked",Toast.LENGTH_SHORT).show();
                    // Checked the switch programmatically
        //            sw1.setChecked(false);
        //        }else{Toast.makeText(getActivity(),"sw1 checked",Toast.LENGTH_SHORT).show();
                    // Checked the switch programmatically
        //            sw1.setChecked(true);}
        //        return false;
        //    }
        //});

        d1.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                if(d1.isChecked())
                {
                    d1.setChecked(false);
                    d2.setChecked(true);
                }else {
                    d1.setChecked(true);
                    d2.setChecked(false);
                }
                return false;
            }
        });

        d2.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                if(d2.isChecked())
                {
                    d2.setChecked(false);
                    d1.setChecked(true);
                }else {
                    d2.setChecked(true);
                    d1.setChecked(false);
                }
                return false;
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.Black));
    }


}
