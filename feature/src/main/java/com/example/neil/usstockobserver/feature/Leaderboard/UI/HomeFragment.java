package com.example.neil.usstockobserver.feature.Leaderboard.UI;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.example.neil.usstockobserver.feature.JsonPlaceHolderApi;
import com.example.neil.usstockobserver.feature.Leaderboard.Data.User;
import com.example.neil.usstockobserver.feature.Leaderboard.InjectorUtils;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.MyTableAdapter;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.MyTableViewListener;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.viewmodel.MainViewModel;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.viewmodel.MainViewModelFactory;
import com.example.neil.usstockobserver.feature.Leaderboard.model.ServiceRequest;
import com.example.neil.usstockobserver.feature.MainFrameActivity;
import com.example.neil.usstockobserver.feature.R;
import com.example.neil.usstockobserver.feature.SearchActivity;
import com.example.neil.usstockobserver.feature.SetPrefActivity;
import com.example.neil.usstockobserver.feature.SharedViewModel;
import com.example.neil.usstockobserver.feature.WholeStockResponse;
import com.github.mikephil.charting.charts.CandleStickChart;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import android.widget.SearchView;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    private static final String SingleStock_Action = "getdtnodata";
    private static final String WholeStock_Action = "getdtnodata";
    private static final int SingleStock_Daily_DtNo = 4507402;
    private static final int SingleStock_Weekly_DtNo = 4507408;
    private static final int SingleStock_Quartly_DtNo = 4507409;
    private static final int SingleStock_Info_DtNo = 4507420;
    private static final int WholeStock_Info_DtNo = 4511001;
    private static final String SingleStock_Daily_ParamStr = "DTMode=0;DTRange=250;DTOrder=2;MajorTable=M783;MTPeriod=0;AssignID=";
    private static final String SingleStock_Weekly_ParamStr = "DTMode=0;DTRange=60;DTOrder=2;MajorTable=M784;MTPeriod=0;AssignID=";
    private static final String SingleStock_Quartly_ParamStr = "DTMode=0;DTRange=60;DTOrder=2;MajorTable=M712;MTPeriod=3;AssignID=";
    private static final String SingleStock_Info_ParamStr = "DTMode=0;DTRange=1;DTOrder=1;MajorTable=M682;MTPeriod=4;AssignID=";
    private static final String WholeeStock_ParamStr = "SPMode=0;DTMode=0;";
    private static final int SingleStock_FilterNo = 0;
    private static final int WholeStock_FilterNo = 0;
    private static final String SingleStock_KeyMap = "";
    private static final String WholeStock_KeyMap = "";
    private static final String SingleStock_AssignSPID = "";
    private static final String WholeStock_AssignSPID = "";
    private final String stockServiceUrl = "https://www.cmoney.tw/MobileService/ashx/";
    private ShareActionProvider shareActionProvider;
    private SharedViewModel sharedViewModel;

    private TableView mTableView;
    private MyTableAdapter mTableAdapter;
    private ProgressBar mProgressBar;
    private MainViewModel vMainViewModel;

    private boolean mRetrofitLoginTaskRunning;
    private TextView mtextView;
    private CandleStickChart mCandleStickChart;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedViewModel = ViewModelProviders.of(getActivity(), InjectorUtils.getSharedViewModelFactory(getContext())).get(SharedViewModel.class);
        sharedViewModel.selectCurrentStock("AMZN");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mProgressBar = view.findViewById(R.id.progressBar);

//        SharedPreferences setting_pref = getActivity().getSharedPreferences("duration", MODE_PRIVATE);
//        setting_pref.edit();
//        //Boolean duration1=setting_pref.getBoolean("duration1",false);
//
//    if(setting_pref.getBoolean("duration1",false)){
//        mTableView.hideColumn(5);
//        mTableView.hideColumn(7);
//    }

//        if(set.booleanValue()==true && key.equals("duration1")){
//            mTableView.hideColumn(5);
//            mTableView.hideColumn(7);
//        }

        mTableView = view.findViewById(R.id.my_TableView);

        initializeTableView(mTableView);


        // initialize ViewModel
        MainViewModelFactory factory = InjectorUtils.getMainViewModelFactory(getActivity().getApplicationContext());
        vMainViewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);

        vMainViewModel.getUserList().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {

                if (users != null && users.size() > 0) {
                    // set the list on TableViewModel
                    mTableAdapter.setUserList(users);
                    HomeFragment.this.hideProgressBar();
                }
            }
        });

        vMainViewModel.getWholeStockDataSet().observe(this, new Observer<ArrayList<ArrayList<String>>>() {
            @Override
            public void onChanged(@Nullable ArrayList<ArrayList<String>> wholeStockDataSet) {

                if (wholeStockDataSet != null && wholeStockDataSet.size() > 0) {
                    // set the list on TableViewModel
                    mTableAdapter.setWholeStockDataSet(wholeStockDataSet);
                    HomeFragment.this.hideProgressBar();

//                    SharedPreferences spref=getActivity().getSharedPreferences("set",MODE_PRIVATE);
//                    //SharedPreferences.Editor editor=spref.edit();
//
//                    if(spref.getBoolean("duration1",true)){
//                        mTableView.hideColumn(5);
//                    }

//                    SharedPreferences setting_pref = getActivity().getSharedPreferences("duration", MODE_PRIVATE);
//                    setting_pref.edit();
//                    //Boolean duration1=setting_pref.getBoolean("duration1",false);
//
//                    if(setting_pref.getBoolean("duration1",false)){
//                        Toast.makeText(getContext(),
//                "value"+setting_pref,
//                Toast.LENGTH_LONG).show();
//                        mTableView.hideColumn(5);
//                        mTableView.hideColumn(7);

                }
            }
        });


        // Let's post a request to get the User data from a web server.
        //TODO: Add Retrofit logic into Repository for postRequest to fetch data
        postRequest();

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true); //讓menu顯示出來
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        //TODO: TO ENABLE TABLEVIEW DATA SOURCE CALL DEBUG
        //getWholeStockData(stockServiceUrl);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.toolbar_menu, menu);

    }


    private void setShareActionIntent(String text) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text);
        shareActionProvider.setShareIntent(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_add_setting) {//Intent intent = new Intent(getActivity(), OrderActivity.class);
            MainFrameActivity mainframeactivity = (MainFrameActivity) getActivity();
            Intent intent = new Intent((MainFrameActivity) getActivity(), SetPrefActivity.class);
            startActivity(intent);
            //mainframeactivity.findViewById(R.id.exit_btn).setVisibility(View.VISIBLE);
//            MyPrefFragment myFragment =new MyPrefFragment();
//            mainframeactivity.setFragmentHelper(myFragment,"MyPrefFragment");
            //Toast.makeText(getContext(), "WEEEEEEEEEEEE!", Toast.LENGTH_SHORT).show();
            return super.onOptionsItemSelected(item);
        }
        if(i==R.id.stock_search){
            MainFrameActivity mainframeactivity = (MainFrameActivity) getActivity();
            Intent intent = new Intent((MainFrameActivity) getActivity(), SearchActivity.class);
            startActivity(intent);
            return super.onOptionsItemSelected(item);
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_add_alert:
//                //Intent intent = new Intent(getActivity(), OrderActivity.class);
//                Toast.makeText(getContext(), "WEEEEEEEEEEEE!", Toast.LENGTH_SHORT).show();
//                return super.onOptionsItemSelected(item);
//            default:
//                return super.onOptionsItemSelected(item);
//
//        }
//    }

    private void getWholeStockData(String serviceUrl){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<WholeStockResponse> wholeStockResponseCall = jsonPlaceHolderApi.getWholeStock(
          WholeStock_Action, WholeStock_Info_DtNo, WholeeStock_ParamStr, WholeStock_FilterNo, WholeStock_KeyMap, WholeStock_AssignSPID
        );

        wholeStockResponseCall.enqueue(new Callback<WholeStockResponse>() {
            @Override
            public void onResponse(Call<WholeStockResponse> call, Response<WholeStockResponse> response) {
                WholeStockResponse wholeStockResponse = response.body();

                if (!response.isSuccessful()){
                    Log.d("Login", ("Code" + response.code()));
                    return;
                }
                ArrayList<String> titles = wholeStockResponse.getTitle();
                ArrayList<ArrayList<String>> dataSet = wholeStockResponse.getDataSet();
                parseWholeStockData(titles, dataSet);
            }

            private void parseWholeStockData(ArrayList<String> titles, ArrayList<ArrayList<String>> dataSet) {
                final int index_name = 0;
                final int index_TickerSymbols = 1;
                final int index_LS1_Close_Price = 2;
                final int index_LS2_Close_Price = 3;
                final int index_LS3_Close_Price = 4;
                final int index_LS4_Close_Price = 5;
                final int index_LS5_Close_Price = 6;
                final int index_LS1_delta_Price = 7;
                final int index_LS2_delta_Price = 8;
                final int index_LS3_delta_Price = 9;
                final int index_LS4_delta_Price = 10;
                final int index_LS5_delta_Price = 11;
                final int index_LY1_Close_Price = 12;
                final int index_LY2_Close_Price = 13;
                final int index_LY3_Close_Price = 14;
                final int index_LY1_delta_Price = 15;
                final int index_LY2_delta_Price = 16;
                final int index_LY3_delta_Price = 17;
                final int index_Sectored_Category_Id_Tier1 = 18;
                final int index_Sectored_Category_Name_Tier1 = 19;
                final int index_Sectored_Category_Name_ZH_Tier1 = 20;
                final int index_Sectored_Category_Id_Tier2 = 21;
                final int index_Sectored_Category_Tier2 = 22;
                final int index_Sectored_Category_Name_ZH_Tier2 = 23;
                final int index_LS1_GPM = 24;
                final int index_LS2_GPM = 25;
                final int index_LS3_GPM = 26;
                final int index_LS1_Net_Profit_Margin = 27;
                final int index_LS2_Net_Profit_Margin = 28;
                final int index_LS3_Net_Profit_Margin = 29;
                final int index_LS1_Operating_Profit_Margin = 30;
                final int index_LS2_Operating_Profit_Margin = 31;
                final int index_LS3_Operating_Profit_Margin = 32;
                final int index_LS1_ROE = 33;
                final int index_LS2_ROE = 34;
                final int index_LS3_ROE = 35;
                final int index_LS1_Current_Ratio = 36;
                final int index_LS2_Current_Ratio = 37;
                final int index_LS3_Current_Ratio = 38;
                final int index_LS1_Inventory_Turnover = 39;
                final int index_LS2_Inventory_Turnover = 40;
                final int index_LS3_Inventory_Turnover = 41;
                final int index_LS1_Total_Assets_Turnover = 42;
                final int index_LS2_Total_Assets_Turnover = 43;
                final int index_LS3_Total_Assets_Turnover = 44;

                ArrayList<ArrayList<String>> filteredDataSet = new ArrayList<>();
                ArrayList<ArrayList<String>> Empty_LD1_Closed_DataSet = new ArrayList<>();
                //TODO: Remove ETF and Funds filtered results after testing;
                ArrayList<ArrayList<String>> ETF_Fund_DataSet = new ArrayList<>();

                //TODO: Parsing Array and filter out data of mutual fund and ETF;;
                for (ArrayList<String> data: dataSet) {
                    boolean IsStock = false;
                    for (int i = index_LS1_GPM; i<index_LS3_Total_Assets_Turnover+1; i++) {
                        if (!data.get(i).equals("") ){
                            IsStock = true;
                            break;
                        }
                    }
                    if (IsStock) {
                        filteredDataSet.add(data);
                        if (!data.get(2).equals("")) {
                            Empty_LD1_Closed_DataSet.add(data);
                        }
                    } else {
                        ETF_Fund_DataSet.add(data);
                    }
                }
                if (getContext() != null){
                    Toast.makeText(getContext(), "原始筆數含ETF和基金: " + dataSet.size()
                                    + "\n僅美股筆數: " + filteredDataSet.size()
                                    + "\n個股昨日收盤價不為空: " + Empty_LD1_Closed_DataSet.size()
                            , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WholeStockResponse> call, Throwable t) {

            }
        });
    }

    private void initializeTableView(TableView tableView) {

        // Create TableView Adapter
        mTableAdapter = new MyTableAdapter(getContext());
        tableView.setAdapter(mTableAdapter);

        // Create listener
        tableView.setTableViewListener(new MyTableViewListener(tableView, sharedViewModel));
    }


    private void postRequest() {
        int size = 100; // this is the count of the data items.
        int page = 1; // Which page do we want to get from the server.
        ServiceRequest serviceRequest = new ServiceRequest(size, page);
        //vMainViewModel.postRequest(serviceRequest);
        vMainViewModel.postWholeStockServiceRequest();

        showProgressBar();
    }


    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        mTableView.setVisibility(View.INVISIBLE);
    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mTableView.setVisibility(View.VISIBLE);
    }
}
