package com.example.neil.usstockobserver.feature.Leaderboard.Data.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.neil.usstockobserver.feature.Leaderboard.AppExecutors;
import com.example.neil.usstockobserver.feature.Leaderboard.Data.User;
import com.example.neil.usstockobserver.feature.Leaderboard.model.ServiceRequest;
import com.example.neil.usstockobserver.feature.StockAlertItem;
import com.example.neil.usstockobserver.feature.StockItem;

import java.util.ArrayList;
import java.util.List;

/**
 * This class responsible for handling data operations. This is the mediator between different
 * data sources (persistent model, web service, cache, etc.)
 */
public class UserRepository {
    private static final String LOG_TAG = UserRepository.class.getSimpleName();

    //private UserDao mUserDao;
    private UserNetworkDataSource mNetworkDataSource;

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static UserRepository sInstance;
    private MutableLiveData<String> mCurrentViewStock = new MutableLiveData<>();
    private MutableLiveData<ArrayList<String>> mSavedStockList = new MutableLiveData<>();
    private MutableLiveData<ArrayList<StockAlertItem>> mStockAlertItems = new MutableLiveData<>();
    private LiveData<ArrayList<ArrayList<String>>> mStockList;
    private LiveData<List<User>> mUserList;

    public UserRepository(UserNetworkDataSource networkDataSource, final AppExecutors
            executors) {
        //this.mUserDao = userDao;
        this.mNetworkDataSource = networkDataSource;

        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        mNetworkDataSource.getUserList().observeForever(new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {
                executors.diskIO().execute(new Runnable() {
                    @Override
                    public void run() {

                        Log.d(LOG_TAG, "user table is updating");
                        //mUserDao.updateAll(users);
                    }
                });
            }
        });

        mSavedStockList.observeForever(new Observer<ArrayList<String>>() {
            @Override
            public void onChanged(@Nullable ArrayList<String> savedStockList) {
                // mNetworkDataSource.fetchSavedStockItems(savedStockList);
                postWatchedStockItemsRequest(savedStockList);
            }
        });

        //TODO: Set mediator LiveData
        mNetworkDataSource.getWholeStockDataSet().observeForever(new Observer<ArrayList<ArrayList<String>>>() {
            @Override
            public void onChanged(@Nullable ArrayList<ArrayList<String>> WholeStockDataSet) {
                executors.diskIO().execute(new Runnable() {
                    @Override
                    public void run() {

                        Log.d(LOG_TAG, "user table is updating");
                        //mUserDao.updateAll(users);
                    }
                });
            }
        });
    }

    public static UserRepository getInstance( UserNetworkDataSource
            networkDataSource, AppExecutors executors) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new UserRepository(networkDataSource, executors);
                Log.d(LOG_TAG, "Made new repository");
            }
        }
        return sInstance;
    }

    public LiveData<ArrayList<ArrayList<String>>> getWholeStockDataSet() {
        //TODO: Connect to API
        //return mStockList = new MutableLiveData<>();
        return mStockList = mNetworkDataSource.getWholeStockDataSet();
        //return mUserDao.getUserList();
    }

    public LiveData<List<User>> getUserList() {
        //return mStockList = new MutableLiveData<>();
        return mUserList = mNetworkDataSource.getUserList();
        //return mUserDao.getUserList();
    }

    //TODO: Discuss Visiablity level
    public void postWatchedStockItemsRequest(ArrayList<String> savedStockList) {
        if (savedStockList.size() == 0 || savedStockList == null) {
            return;
        }
        mNetworkDataSource.fetchSavedStockItems(savedStockList);
    }

    public void postServiceRequest(ServiceRequest serviceRequest) {
        mNetworkDataSource.fetchData(serviceRequest);
    }

    public void postWholeStockServiceRequest() {
        mNetworkDataSource.fetchWholeStockData();
    }

    public MutableLiveData<String> getCurrentViewStock() {
        return mCurrentViewStock;
    }

    public MutableLiveData<ArrayList<StockAlertItem>> getStockAlertItems() {
        return mStockAlertItems;
    }

    public LiveData<ArrayList<StockItem>> getWholeWatchedItems() {
        return mNetworkDataSource.getDownloadedWholeWatchedItems();
    }

    public MutableLiveData<ArrayList<String>> getSavedStockList() {
        return mSavedStockList;
    }
}
