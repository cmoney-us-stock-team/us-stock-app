package com.example.neil.usstockobserver.feature;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface JsonPlaceHolderApi {

    @POST("LoginCheck/LoginCheck.ashx")
    Call<LoginResponse> getLogin(
            @Query("Action")String Action,
            @Query("Account") String Account,
            @Query("token") String PushToken,
            @Query("Password") String HashedPassword,
            @Query("AppID") int AppID,
            @Query("Device") int DeviceCategory,
            @Query("isNeedPush") boolean IsNeededPushService
    );

    @POST("LoginCheck/LoginCheck.ashx")
    Call<LoginResponse> getForgetPassword(
            @Query("Action")String Action,
            @Query("Account") String Account
    );


    @POST("LoginCheck/LoginCheck.ashx")
    Call<LoginResponse> Register(
            @Query("Action")String Action,
            @Query("Account") String Account,
            @Query("Password") String Password,
            @Query("Device") int Device
    );


    @POST("LoginCheck/LoginCheck.ashx")
    Call<MemberProfileResponse> getMemberInfo(
            @Query("Action")String Action,
            @Query("Guid") String Guid,
            @Query("AuthToken") String AuthToken,
            @Query("AppID") int AppID
    );

    @GET("GetDtnoData.ashx")
    Call<SingleStockDataResponse>getSingleStock(
        @Query("Action")String Action,
        @Query("DtNo") int DtNo,
        //@Query(value="ParamStr", encoded=true) String ParamStr,
        @Query("ParamStr") String ParamStr,
        @Query("FilterNo") int FilterNo,
        @Query("KeyMap") String KeyMap,
        @Query("AssignSPID") String AssignSPID
    );


    @GET("GetDtnoData.ashx")
    Call<WholeStockResponse>getWholeStock(
            @Query("Action")String Action,
            @Query("DtNo") int DtNo,
            //@Query(value="ParamStr", encoded=true) String ParamStr,
            @Query("ParamStr") String ParamStr,
            @Query("FilterNo") int FilterNo,
            @Query("KeyMap") String KeyMap,
            @Query("AssignSPID") String AssignSPID
    );

    @GET("LoginCheck/LoginCheck.ashx")
    Call<ForgetPasswordResponse> getForgetpassward(
            @Query("Action")String Action,
            @Query("Account") String Account
    );

//    @FormUrlEncoded
//    @POST("GetDtnoData.ashx")
//    Call<SingleStockDataResponse>getSingleStock(
//        @Field("Action")String Action,
//        @Field("DtNo") int DtNo,
//        @Field(value="ParamStr", encoded=false) String ParamStr,
//        //@Field("ParamStr") String ParamStr,
//        @Field("FilterNo") int FilterNo,
//        @Field("AssignID") String AssignID,
//        @Field("KeyMap") String KeyMap,
//        @Field("AssignSPID") String AssignSPID
//    );

}
