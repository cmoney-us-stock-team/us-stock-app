//package com.example.neil.usstockobserver.feature;
//
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.helper.ItemTouchHelper;
//
//public class RecyclerviewItemTouchHelperCallback extends ItemTouchHelper.Callback {
//
//    //刪除和滑動的介面
//    public interface ItemMoveSwipeListener {
//        boolean onItemMove(int fromPosition, int toPosition);
//        void onItemSwipe(int position);
//    }
//
//    private ItemMoveSwipeListener itemMoveSwipeListener;
//
//    public RecyclerviewItemTouchHelperCallback(ItemMoveSwipeListener itemMoveSwipeListener){
//        this.itemMoveSwipeListener = itemMoveSwipeListener;
//    }
//
//    @Override
//    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
//        //上下移動
//        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
//        //不要左右移動
//        int swipeFlags =0;
//        return makeMovementFlags(dragFlags, swipeFlags);
//    }
//
//    //移動後要做什麼事
//    @Override
//    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
//        //實作介面
//        return itemMoveSwipeListener.onItemMove(viewHolder.getAdapterPosition(), viewHolder1.getAdapterPosition());
//    }
//
//    //滑動後要做什麼事
//    @Override
//    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
//        itemMoveSwipeListener.onItemSwipe(viewHolder.getAdapterPosition());
//    }
//}
