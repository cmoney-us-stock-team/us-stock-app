package com.example.neil.usstockobserver.feature;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<com.example.neil.usstockobserver.feature.MyAdapter.ViewHolder> {

    private List<String> mStock;
    private ArrayList<StockItem> stockItemList;
    private SharedViewModel mSharedViewModel;

    public MyAdapter(List<String> stock) {
        this.mStock = stock;
    }

    public MyAdapter(ArrayList<StockItem> stockItemList, SharedViewModel sharedViewModel) {
        this.stockItemList = stockItemList;
        this.mSharedViewModel = sharedViewModel;
    }

//        public void removeItem(int position){
//            mStock.remove(position);
//            notifyItemRemoved(position);
//        }

    // 建立ViewHolder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // 宣告元件
        private TextView txt_StockSymbol;
        private TextView txt_StcokName;
        private TextView txt_DailyClosedPrice;
        private TextView txt_Sector;
        private Button btnRemove;
        private ImageView imageRemove;

        public ViewHolder(final View itemView) {
            super(itemView);
            txt_StockSymbol = (TextView) itemView.findViewById(R.id.txt_StockSymbol);
            txt_StcokName = (TextView) itemView.findViewById(R.id.txt_StockItemName);
            txt_DailyClosedPrice = (TextView) itemView.findViewById(R.id.txt_DailyClosedPrice);
            txt_Sector = (TextView) itemView.findViewById(R.id.txt_Sector);
            //  btnRemove = (Button) itemView.findViewById(R.id.btnRemove);
            imageRemove = (ImageView) itemView.findViewById(R.id.Imagedelet);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                        Toast.makeText(view.getContext(),
//                                "click " +getAdapterPosition(), Toast.LENGTH_SHORT).show();
//                        MainFrameActivity frameActivity = ;
//                        frameActivity.setFragmentHelper(new SingleStockPagerFragment(), "SingleStockPagerFragment");
                }
            });

            imageRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(itemView.getContext());
                    dialog.setTitle(R.string.delete_MyStock);
                    dialog.setNegativeButton("否", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    dialog.setPositiveButton("是", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            removeData(getAdapterPosition());
                        }
                    });
                    dialog.show();
                }
            });

            txt_StcokName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String stockSymbol = txt_StockSymbol.getText().toString();
                    if (!stockSymbol.isEmpty()) {
                        mSharedViewModel.selectCurrentStock(stockSymbol);
                        Intent stockIntent = new Intent(v.getContext(), SingleStockActivity.class);
                        v.getContext().startActivity(stockIntent);
                    }
                }
            });

            txt_StockSymbol.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String stockSymbol = txt_StockSymbol.getText().toString();
                    if (!stockSymbol.isEmpty()) {
                        mSharedViewModel.selectCurrentStock(stockSymbol);
                        Intent stockIntent = new Intent(v.getContext(), SingleStockActivity.class);
                        v.getContext().startActivity(stockIntent);
                    }
                }
            });
//                btnRemove.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        AlertDialog.Builder dialog = new AlertDialog.Builder(itemView.getContext());
//                        dialog.setTitle(R.string.delete_MyStock);
//                        dialog.setNegativeButton("否", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//
//                            }
//                        });
//                        dialog.setPositiveButton("是", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        removeData(getAdapterPosition());
//                                    }
//                                });
//                        dialog.show();
//
//                        // 移除項目，getAdapterPosition為點擊的項目位置
//                        //removeData(getAdapterPosition());
//                    }
//                });
//
//                itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                    @Override
//                    public boolean onLongClick(View v) {
//                        AlertDialog.Builder dialog = new AlertDialog.Builder(itemView.getContext());
//                        dialog.setTitle(R.string.delete_MyStock);
//                        dialog.setNegativeButton("否", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//
//                            }
//                        });
//                        dialog.setPositiveButton("是", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        removeData(getAdapterPosition());
//                                    }
//                                });
//                        dialog.show();
//                        return false;
//                    }
//                });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycleview_my_stock, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // 設置txtItem要顯示的內容
        //holder.txt_StockSymbol.setText(mStock.get(position));

        StockItem currentItem = stockItemList.get(position);
        holder.txt_StockSymbol.setText(currentItem.getStockSymbol());
        holder.txt_StcokName.setText(currentItem.getStockName());
        holder.txt_DailyClosedPrice.setText(currentItem.getDailyClosedPrice());
        holder.txt_Sector.setText(currentItem.getSector());
    }

    @Override
    public int getItemCount() {
        //return mStock.size();
        return stockItemList.size();
    }


    public void removeData(int position) {
        //mStock.remove(position);
        stockItemList.remove(position);
        //刪除動畫
        notifyItemRemoved(position);
        notifyDataSetChanged();
        nofityCurrentList();
    }

    private void nofityCurrentList() {
        ArrayList<String> stockList = new ArrayList<>();
        for (StockItem item : stockItemList) {
            String stockSymbol = item.getStockSymbol();
            if (!stockSymbol.isEmpty()) {
                stockList.add(stockSymbol);
            }
        }
        mSharedViewModel.setSavedStockList(stockList);
    }

}


