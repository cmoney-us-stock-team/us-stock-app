package com.example.neil.usstockobserver.feature;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.neil.usstockobserver.feature.Leaderboard.InjectorUtils;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.HomeFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainFrameActivity extends AppCompatActivity {

    private final String PreferencesDataName = "MyStock";
    private final String TAG_STOCK_LIST = "stock_list";
    private TextView mTextMessage;
    public boolean notification_control;
    final Fragment fragment1=new HomeFragment();
    final Fragment fragment2=new MyStockPagerFragment();
    final Fragment fragment3=new NotificationFragment();
    final Fragment fragment4= new MemberProfileFragment();
    final FragmentManager fm=getSupportFragmentManager();
    private SharedViewModel mSharedViewModel;
    Fragment active = fragment1;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.navigation_home) {
                //mTextMessage.setText(R.string.title_home);
                fm.beginTransaction().hide(active).show(fragment1).commit();
                active=fragment1;
                return true;
            } else if (id == R.id.navigation_bookmarked) {
                //mTextMessage.setText(R.string.title_dashboard);
                fm.beginTransaction().hide(active).show(fragment2).commit();
                active = fragment2;
                return true;
            }else if (id == R.id.navigation_notifications) {
                fm.beginTransaction().hide(active).show(fragment3).commit();
                active=fragment3;
                return true;
            } else if (id == R.id.navigation_person) {
                //mTextMessage.setText(R.string.title_notifications);
                //setFragmentHelper(new SingleStockPagerFragment(), "SingleStockPagerFragment");
                fm.beginTransaction().hide(active).show(fragment4).commit();
                active=fragment4;
                //setFragmentHelper(new MemberProfileFragment(), "MemberProfileFragment");
                return true;
            }
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_frame);
        mSharedViewModel = ViewModelProviders.of(this, InjectorUtils.getSharedViewModelFactory(getBaseContext())).get(SharedViewModel.class);
        LiveDataLoader();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    //fm.beginTransaction().add(R.id.frame,)
    fm.beginTransaction().add(R.id.frame, fragment4, "4").hide(fragment4).commit();
    fm.beginTransaction().add(R.id.frame, fragment3, "3").hide(fragment3).commit();
    fm.beginTransaction().add(R.id.frame, fragment2, "2").hide(fragment2).commit();
    fm.beginTransaction().add(R.id.frame, fragment1, "1").commit();
    //setFragmentHelper(new HomeFragment(), "HomeFragment");

    }

    @Override
    protected void onPause() {
        super.onPause();
        ArrayList<String> savedStockList = mSharedViewModel.getSavedStockList().getValue();
        if (savedStockList != null) {
            saveWatchedStockListToJson(savedStockList);
        }
    }

    private void LiveDataLoader() {
        SharedPreferences sharedPreferences = getSharedPreferences(PreferencesDataName, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(TAG_STOCK_LIST, null);
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        ArrayList<String> savedStockSymbolList = gson.fromJson(json, type);
        if (savedStockSymbolList == null) {
            savedStockSymbolList = new ArrayList<>();
            savedStockSymbolList.add("AAPL");
            savedStockSymbolList.add("AMZN");
            savedStockSymbolList.add("MMM");
        }
        mSharedViewModel.setSavedStockList(savedStockSymbolList);
//        SharedPreferences pref = getActivity().getSharedPreferences("MyStock1", Context.MODE_PRIVATE);
//        String name = pref.getString("name", "");
//        .setText(name);
    }

    private void saveWatchedStockListToJson(ArrayList<String> savedStockSymbolList) {
        SharedPreferences sharedPreferences = getSharedPreferences(PreferencesDataName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(savedStockSymbolList);
        System.out.println(json);
        editor.putString(TAG_STOCK_LIST, json);
        editor.apply();
    }


    public void setFragmentHelper(Fragment fragment, String fragmentName){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment, fragmentName);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        //getSupportFragmentManager().executePendingTransactions();
    }

    public void setFragmentHelper(Fragment fragment, int framID, String fragmentName){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(framID, fragment, fragmentName);
        fragmentTransaction.commit();
        //getSupportFragmentManager().executePendingTransactions();
    }


}
