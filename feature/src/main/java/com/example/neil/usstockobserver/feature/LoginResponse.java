package com.example.neil.usstockobserver.feature;

public class LoginResponse {
//    private String Action;
//    private String Account;
//    private String token;
//    private String Password;
//    private int AppID;
//    private int Device;
//    private boolean isNeedPush;

    private String Guid;
    private String AuthToken;
    private int MemberPK;
    private boolean IsFirstLogin;
    private int ResponseCode;
    private String ResponseMsg;

//    public String getAction() {
//        return Action;
//    }
//
//    public String getAccount() {
//        return Account;
//    }
//
//    public String getToken() {
//        return token;
//    }
//
//    public String getPassword() {
//        return Password;
//    }
//
//    public int getAppID() {
//        return AppID;
//    }
//
//    public int getDevice() {
//        return Device;
//    }
//
//    public boolean isNeedPush() {
//        return isNeedPush;
//    }

    public String getGuid() {
        return Guid;
    }

    public String getAuthToken() {
        return AuthToken;
    }

    public int getMemberPK() {
        return MemberPK;
    }

    public boolean isFirstLogin() {
        return IsFirstLogin;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public String getResponseMsg() {
        return ResponseMsg;
    }

}
