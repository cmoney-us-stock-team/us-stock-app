package com.example.neil.usstockobserver.feature;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class MemberProfileFragment extends Fragment {

    private final String memberServiceUrl = "https://www.cmoney.tw/MobileService/ashx/";
    private static final int AppID = 2;
    private static final int Device_Android = 2;
    private static final boolean Need_Broadcast = false;
    private static final String Broadcast_Token = "";
    private static final String Action_DefaultLogin = "getloginguid";
    private static final String Action_Forgot_Password = "forgetpassword";
    private static final String Action_Register_Account = "registeraccount";
    private static final String Action_Get_MemberProfile = "getmemberprofile";
    private static final int Login_Response_Successful = 1;
    private static final int Login_Response_AuthedFail = 2;
    private static final int Login_Response_ServerNoResponse = 9002;
    private static final int MemberProfile_ResponseeCode_AuthedFail = 101;
    private static final int MemberProfile_ResponseeCode_Success = 1;
    private static final String sessionName = "session";
    private static final String session_pref_key_Guid = "Guid";
    private static final String session_pref_key_AuthToken = "AuthToken";
    private static final String SESSION_DEFAULT_PREF_RETURN = "Not logged in";


    private boolean mRetrofitLoginTaskRunning = false;

    private String mAuthToken;
    private String mGuid;
    private SharedPreferences mSharedPreferences;
    private ImageView profileImage;
    private TextView mUserNickName;
    private TextView mUserEmail;

    public MemberProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_member_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        profileImage = view.findViewById(R.id.profileImage);
        mUserNickName = view.findViewById(R.id.txt_Nickname);
        mUserEmail = view.findViewById(R.id.txt_Email);
        mSharedPreferences = getActivity().getSharedPreferences(sessionName, Context.MODE_PRIVATE);
        mGuid = mSharedPreferences.getString(session_pref_key_Guid,SESSION_DEFAULT_PREF_RETURN);
        mAuthToken = mSharedPreferences.getString(session_pref_key_AuthToken,SESSION_DEFAULT_PREF_RETURN);
        getMemberProfileTask(memberServiceUrl);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        TextView textView = view.findViewById(R.id.tv_title);
        textView.setText("個人資訊");
    }

    @Override
    public void onStart() {
        super.onStart();
        //TODO: REFACTOR LOGICS
    }


    private void getMemberProfileTask(String serviceUrl){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<MemberProfileResponse> profileResponseCall = jsonPlaceHolderApi.getMemberInfo(
                Action_Get_MemberProfile, mGuid, mAuthToken, AppID
        );

        //TODO: REFACTOR
        profileResponseCall.enqueue(new Callback<MemberProfileResponse>() {
            @Override
            public void onResponse(Call<MemberProfileResponse> call, Response<MemberProfileResponse> response) {
                Intent intent = null;
                MemberProfileResponse memberProfileResponse = response.body();
                mRetrofitLoginTaskRunning = false;

                if (!response.isSuccessful()){
                    Log.d("Login",  ("Code: " + response.code()));
                    Toast.makeText(getActivity(), "Error Response Code: "+response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }

                switch (memberProfileResponse.getResponseCode()){
                    case MemberProfile_ResponseeCode_Success:
                        //On Android P device, http request on default is not allowed
                        String imageUrl = memberProfileResponse.getImagePath().replaceFirst("http:", "https:");
                        //Toast.makeText(getActivity().getApplicationContext(), "Login successfully!", Toast.LENGTH_SHORT).show();
                        //TODO: Picaso
                        //Picasso.get().load(imageUrl).into(profileImage);
                        //TODO: Debug Glide
                        //Glide.with(MemberProfileFragment.this).load(imageUrl).into(profileImage);
                        Glide.with(MemberProfileFragment.this) .load(imageUrl)
                                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE)
                                        .error(R.drawable.ic_observatory)) .into(profileImage);

                        Log.d("Login", imageUrl);
                        //TODO: Add Memberinfo
                        mUserNickName.setText(memberProfileResponse.getNickName());
                        mUserEmail.setText(memberProfileResponse.getEmail());
                        break;
                    case MemberProfile_ResponseeCode_AuthedFail:
                        //TODO: REFACTOR THIS LOGIC
                        break;
                    default:
                        Toast.makeText(getActivity().getApplicationContext(), "Error", Toast.LENGTH_SHORT);
                        break;
                }

                //Toast.makeText(getActivity(), "HTTP Response Code: "+response.code()+ "\n"+ loginResponse.getAuthToken(), Toast.LENGTH_LONG).show();
//                if (intent != null){
//                    startActivity(intent);
//                    finish();
//                }
            }

            @Override
            public void onFailure(Call<MemberProfileResponse> call, Throwable t) {
                Log.d("Login",  ("Failed: " + t.toString()));
                Toast.makeText(getActivity(), "Unable to connect to the server", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
