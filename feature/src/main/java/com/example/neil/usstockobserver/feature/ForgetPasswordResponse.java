package com.example.neil.usstockobserver.feature;

public class ForgetPasswordResponse {

    private int ResponseCode;
    private String ResponseMsg;


    public int getResponseCode() {
        return ResponseCode;
    }

    public String getResponseMsg() {
        return ResponseMsg;
    }

}
