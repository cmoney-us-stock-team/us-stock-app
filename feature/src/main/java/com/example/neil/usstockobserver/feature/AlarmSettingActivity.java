package com.example.neil.usstockobserver.feature;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.neil.usstockobserver.feature.Leaderboard.InjectorUtils;

public class AlarmSettingActivity extends AppCompatActivity {

    RadioGroup RG1, RG2, RG3, RG4, RG5, RG6, RG7;
    CheckBox ckBox1, ckBox2, ckBox3, ckBox4, ckBox5, ckBox6, ckBox7, ckBox8, ckBox9;
    Button btn_checked;
    EditText set_price_H,set_price_L;
    RadioButton radioButton1;
    SharedViewModel sharedViewModel;
    StockAlertItem stockAlertItem;
    private final String default_not_selected_value = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_setting);
        bindView();
        setAllListener();
        sharedViewModel = ViewModelProviders.of(this, InjectorUtils.getSharedViewModelFactory(getBaseContext())).get(SharedViewModel.class);
        sharedViewModel.getSelectedCurrentStock().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String stockSymbol) {

            }
        });
    }

    private void bindView() {
        ckBox1 = (CheckBox) findViewById(R.id.checkBox1);
        ckBox2 = (CheckBox) findViewById(R.id.checkBox2);
        ckBox3 = (CheckBox) findViewById(R.id.checkBox3);
        ckBox4 = findViewById(R.id.checkBox4);
        ckBox5 = findViewById(R.id.checkBox5);
        ckBox6 = findViewById(R.id.checkBox6);
        ckBox7 = findViewById(R.id.checkBox7);
        ckBox8 = findViewById(R.id.checkBox8);
        ckBox9 = findViewById(R.id.checkBox9);
        btn_checked = findViewById(R.id.alarm_checked);
        RG1 = findViewById(R.id.group1);
        RG2 = findViewById(R.id.group2);
        RG3 = findViewById(R.id.group3);
        RG4 = findViewById(R.id.group4);
        RG5 = findViewById(R.id.group5);
        RG6 = findViewById(R.id.group6);
        RG7 = findViewById(R.id.group7);
        set_price_H = findViewById(R.id.set_price_H);
        set_price_L = findViewById(R.id.set_price_L);
        radioButton1 = findViewById(RG1.getCheckedRadioButtonId());
    }

    private void setAllListener() {
        ckBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    RG1.setVisibility(View.VISIBLE);
                    //Toast.makeText(getActivity(), "radioButton1", Toast.LENGTH_LONG).show();
                } else
                    RG1.setVisibility(View.GONE);
            }
        });

        ckBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    RG2.setVisibility(View.VISIBLE);
                    //switch(RG2.getCheckedRadioButtonId()){
                    //    case R.id.radioButton2_1:
                    //        break;
                    //    case R.id.radioButton2_2:
                    //        break;
                    //    case R.id.radioButton2_3:
                    //        break;}
                } else
                    RG2.setVisibility(View.GONE);
            }
        });

        ckBox3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    RG3.setVisibility(View.VISIBLE);
                    //switch(RG3.getCheckedRadioButtonId()){
                    //    case R.id.radioButton3_1:
                    //        break;
                    //    case R.id.radioButton3_2:
                    //        break;
                    //    case R.id.radioButton3_3:
                    //        break;}
                } else
                    RG3.setVisibility(View.GONE);
            }
        });

        ckBox4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    RG4.setVisibility(View.VISIBLE);
                    //switch(RG4.getCheckedRadioButtonId()){
                    //    case R.id.radioButton4_1:
                    //        break;
                    //    case R.id.radioButton4_2:
                    //        break;
                    //    case R.id.radioButton4_3:
                    //        break;}
                } else
                    RG4.setVisibility(View.GONE);
            }
        });

        ckBox5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    RG5.setVisibility(View.VISIBLE);
                    //switch(RG5.getCheckedRadioButtonId()){
                    //    case R.id.radioButton5_1:
                    //        break;
                    //    case R.id.radioButton5_2:
                    //        break;
                    //    case R.id.radioButton5_3:
                    //        break;}
                } else
                    RG5.setVisibility(View.GONE);
            }
        });

        ckBox6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    RG6.setVisibility(View.VISIBLE);
                    //switch(RG6.getCheckedRadioButtonId()){
                    //    case R.id.radioButton6_1:
                    //        break;
                    //    case R.id.radioButton6_2:
                    //        break;
                    //    case R.id.radioButton6_3:
                    //        break;}
                } else
                    RG6.setVisibility(View.GONE);
            }
        });

        ckBox7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    RG7.setVisibility(View.VISIBLE);
                    //switch(RG7.getCheckedRadioButtonId()){
                    //    case R.id.radioButton7_1:
                    //        break;
                    //    case R.id.radioButton7_2:
                    //        break;
                    //    case R.id.radioButton7_3:
                    //        break;}
                } else
                    RG7.setVisibility(View.GONE);
            }
        });

        ckBox8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    set_price_H.setVisibility(View.VISIBLE);
                else
                    set_price_H.setVisibility(View.GONE);
            }
        });

        ckBox9.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    set_price_L.setVisibility(View.VISIBLE);
                else
                    set_price_L.setVisibility(View.GONE);
            }
        });

        RG1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton1_1) {
                    Toast.makeText(getBaseContext(), "毛利率3%", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.radioButton1_2) {
                    Toast.makeText(getBaseContext(), "rb2", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.radioButton1_3) {
                    Toast.makeText(getBaseContext(), "rb3", Toast.LENGTH_SHORT).show();
                }
            }
        });

        RG2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton2_1) {

                } else if (checkedId == R.id.radioButton2_2) {

                } else if (checkedId == R.id.radioButton2_3) {

                }
            }
        });

        RG3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton3_1) {

                } else if (checkedId == R.id.radioButton3_2) {

                } else if (checkedId == R.id.radioButton3_3) {

                }
            }
        });

        RG4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton4_1) {

                } else if (checkedId == R.id.radioButton4_2) {

                } else if (checkedId == R.id.radioButton4_3) {

                }
            }
        });

        RG5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton5_1) {

                } else if (checkedId == R.id.radioButton5_2) {

                } else if (checkedId == R.id.radioButton5_3) {

                }
            }
        });

        RG6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton6_1) {

                } else if (checkedId == R.id.radioButton6_2) {

                } else if (checkedId == R.id.radioButton6_3) {

                }
            }
        });

        RG7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton7_1) {

                } else if (checkedId == R.id.radioButton7_2) {

                } else if (checkedId == R.id.radioButton7_3) {

                }
            }
        });

        btn_checked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
