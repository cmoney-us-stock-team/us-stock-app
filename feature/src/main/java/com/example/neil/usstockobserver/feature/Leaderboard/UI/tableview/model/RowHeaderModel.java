package com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.model;

/**
 * Created by evrencoskun on 27.11.2017.
 */

public class RowHeaderModel {
    private String mData;
    private String mStockSymbol;
    private String mUnSortedRowIndex;

    public RowHeaderModel(String mData) {
        this.mData = mData;
    }

    public RowHeaderModel(String mData, String mStockSymbol) {
        this.mData = mData;
        this.mStockSymbol = mStockSymbol;
    }

    public String getStockSymbol() {
        return mStockSymbol;
    }

    public String getData() {
        return mData;
    }
}
