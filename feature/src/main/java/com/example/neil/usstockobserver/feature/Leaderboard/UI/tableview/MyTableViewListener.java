package com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.evrencoskun.tableview.ITableView;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.holder.CellViewHolder;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.holder.ColumnHeaderViewHolder;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.holder.RowHeaderViewHolder;
import com.example.neil.usstockobserver.feature.Leaderboard.UI.tableview.popup.ColumnHeaderPressPopup;
import com.example.neil.usstockobserver.feature.SharedViewModel;
import com.example.neil.usstockobserver.feature.SingleStockActivity;

import static com.github.mikephil.charting.charts.Chart.LOG_TAG;

/**
 * Created by evrencoskun on 2.12.2017.
 */

public class MyTableViewListener implements ITableViewListener {

    private ITableView mTableView;
    private SharedViewModel sharedViewModel;

    public MyTableViewListener(ITableView pTableView, SharedViewModel sharedViewModel) {
        this.mTableView = pTableView;
        this.sharedViewModel = sharedViewModel;
    }


    //TODO:ADD sharedViewModel logic
    @Override
    public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {
        //引入fragment
        //傳代碼到下一個fragment
        //((MainFrameActivity)(cellView.itemView.getContext())).getSupportFragmentManager().
        //TODO: Switch Activity;
        Log.d(LOG_TAG, "onCellClicked has been clicked for x= " + column + " y= " + row);
        if (cellView != null && cellView instanceof CellViewHolder && mTableView.getAdapter() instanceof MyTableAdapter) {
            MyTableAdapter myTableAdapter = (MyTableAdapter) mTableView.getAdapter();
            ((CellViewHolder) cellView).setSelected(AbstractViewHolder.SelectionState.SHADOWED);

            //Get the view text from clicked cell
            String cellText = ((CellViewHolder) cellView).cell_textview.getText().toString();

            //Get the StockSymbol from the textview's tag
            String stockSymbol = ((CellViewHolder) cellView).cell_textview.getTag().toString();

/*
            //Get the RowHeader Item of the cell's row (if it's in origin state).
            //However, during sorted state, the row from onCellClicked is not the actual row for the items in adapter
            //This is because the items is from the original list, hence the row may not matching the current status.
            RowHeaderModel rowHeaderItem = myTableAdapter.getRowHeaderItem(row);
            //Get the data from the item
            String rownumber = rowHeaderItem.getData();
            String stockSymbol2 = rowHeaderItem.getStockSymbol();

            //Get the row header's view holder
            RowHeaderViewHolder rowHeaderViewHolder = (RowHeaderViewHolder) mTableView.getRowHeaderRecyclerView().findViewHolderForAdapterPosition(row);
            // if the row header is index-1, then you can extract the view text as the actual cell row index in the adapter
            // int dataRow = Integer.valueOf(rowHeaderViewHolder.row_header_textview.getText().toString()) ;

            //Get the list of the items from that column
            List columnItems = mTableView.getAdapter().getCellRecyclerViewAdapter().getColumnItems(column);

            int dataRow = rowHeaderViewHolder.getAdapterPosition();
            int adpterPosition = ((CellViewHolder) cellView).getAdapterPosition();

            //We can also use the custom method from CellModel to get the stock symbol;
            CellModel cellModel = myTableAdapter.getCellItem(1, dataRow);
            String symbol = ((CellModel) cellModel).getStockSymbol();
*/

            //Now Set Value to livedata to dispatch results
            //TODO: ENABLE TO SEE TABLEVIEW onCellClicked
            sharedViewModel.selectCurrentStock(stockSymbol);
//            Toast.makeText(cellView.itemView.getContext(),
//                    "此格為: " + cellText
////                            + "\nCellAdapterPosition: " + adpterPosition
////                            + "\nRowHeaderItem: " + rownumber
////                            + "\nDataRow: " + dataRow
//                            + "\n取得代碼: " + stockSymbol
//                    , Toast.LENGTH_SHORT).show();
            //TODO: Change livedata only if column content is company name
            if (column == 0) {
                sharedViewModel.selectCurrentStock(stockSymbol);
                Intent stockIntent = new Intent(cellView.itemView.getContext(), SingleStockActivity.class);
                cellView.itemView.getContext().startActivity(stockIntent);
            }

        }

    }


    @Override
    public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {
    }

    @Override
    public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int
            column) {
        if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {

            // Create Long Press Popup
            ColumnHeaderPressPopup popup = new ColumnHeaderPressPopup(
                    (ColumnHeaderViewHolder) columnHeaderView, mTableView);

            // Show
            popup.show();
        }
    }

    @Override
    public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int
            column) {
    }

    @Override
    public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
        if (rowHeaderView != null && rowHeaderView instanceof RowHeaderViewHolder) {
            String rowHeader = ((RowHeaderViewHolder) rowHeaderView).row_header_textview.getText().toString();
//            Toast.makeText(rowHeaderView.itemView.getContext(),
//                    "此格為: " + rowHeader
//                            + "\nRow: " + row
//                    , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder owHeaderView, int row) {
    }
}
