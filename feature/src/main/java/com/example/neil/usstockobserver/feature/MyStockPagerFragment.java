package com.example.neil.usstockobserver.feature;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyStockPagerFragment extends Fragment {

    public int currentPage;

    public MyStockPagerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_stock_pager, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setHasOptionsMenu(true);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        TextView textView = view.findViewById(R.id.tv_title);
        textView.setText("自選股");

        //隱藏toolbar項目
//        Menu menu = toolbar.getMenu();
//        MenuItem item = menu.findItem(R.id.action_add_setting);
//        item.setVisible(false);

        //將sectionPagerAdapter指派給ViewPager  一個新的類別
        SectionsPagerAdapter pagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        ViewPager pager = (ViewPager) getView().findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);

        //將ViewPager指派給Tablayot
        TabLayout tabLayout = (TabLayout) getView().findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        //建構子
        public SectionsPagerAdapter(FragmentManager fm){
            super(fm);
        }
        //回傳view Pager的頁數
        @Override
        public int getCount(){
            return 1;
        }

        //指定對應的fragment頁面
        @Override
        public Fragment getItem(int position){
            currentPage=position;
            Log.d("Pager", "Position =" + position);
            switch (position){
                case 0:
                    return new MyStockFragment();
//                case 1:
//                    return new MyStockFragment();
//                case 2:
//                    return new MyStockFragment();
//                case 3:
//                    return new MyStockFragment();
            }
            return null;
        }

        //將文字加到標籤裡
        public CharSequence getPageTitle(int position){
            switch (position){
                case 0:
                    return getResources().getText(R.string.self_select1);
//                case 1:
//                    return getResources().getText(R.string.self_select2);
//                case 2:
//                    return getResources().getText(R.string.self_select3);
//                case 3:
//                    return getResources().getText(R.string.self_select4);
            }
            return null;
        }
    }
}
