package com.example.neil.usstockobserver.feature;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WholeStockResponse {
    @SerializedName("Title")
    private ArrayList<String> title;
    @SerializedName("Data")
    private ArrayList<ArrayList<String>> dataSet;

    public ArrayList<String> getTitle() {
        return title;
    }

    public ArrayList<ArrayList<String>> getDataSet() {
        return dataSet;
    }
}
