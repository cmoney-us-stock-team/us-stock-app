package com.example.neil.usstockobserver.feature;


import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.ShareActionProvider;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.neil.usstockobserver.feature.Leaderboard.InjectorUtils;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.data.CandleDataSet;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class SingleStockFragment extends Fragment {

    private final String PreferencesDataName = "MyStock";
    private final String TAG_STOCK_LIST = "stock_list";

    private static final String SingleStock_Action = "getdtnodata";
    private static final String WholeStock_Action = "getdtnodata";
    private static final int SingleStock_Daily_DtNo = 4507402;
    private static final int SingleStock_Weekly_DtNo = 4507408;
    private static final int SingleStock_Quartly_DtNo = 4507409;
    private static final int SingleStock_Info_DtNo = 4507420;
    private static final int WholeStock_Info_DtNo = 4511001;
    private static final String SingleStock_Daily_ParamStr = "DTMode=0;DTRange=250;DTOrder=2;MajorTable=M783;MTPeriod=0;AssignID=";
    private static final String SingleStock_Weekly_ParamStr = "DTMode=0;DTRange=60;DTOrder=2;MajorTable=M784;MTPeriod=0;AssignID=";
    private static final String SingleStock_Quartly_ParamStr = "DTMode=0;DTRange=60;DTOrder=2;MajorTable=M712;MTPeriod=3;AssignID=";
    private static final String SingleStock_Info_ParamStr = "DTMode=0;DTRange=1;DTOrder=1;MajorTable=M682;MTPeriod=4;AssignID=";
    private static final String WholeStock_ParamStr = "SPMode=0;DTMode=0;";
    private static final int SingleStock_FilterNo = 0;
    private static final int WholeStock_FilterNo = 0;
    private static final String SingleStock_KeyMap = "";
    private static final String WholeStock_KeyMap = "";
    private static final String SingleStock_AssignSPID = "";
    private static final String WholeStock_AssignSPID = "";
    private final String stockServiceUrl = "https://www.cmoney.tw/MobileService/ashx/";
    private ShareActionProvider shareActionProvider;

    private boolean mRetrofitLoginTaskRunning;
    private CandleStickChart mCandleStickChart;
    private TableLayout mStockCandleEntryTable;

    //財報
    private static final String SingleStock_FnReprt_ParamStr = "DTMode=0;DTRange=4;DTOrder=2;MajorTable=M712;MTPeriod=3;AssignID=";
    private TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16;
    private ArrayList<TextView> tvList = new ArrayList<>();
    private TextView mEntryOpen, mEntryClosed, mEntryHigh, mEntryLow;
    private SharedViewModel sharedViewModel;
    private Button btn_update;
    private EditText editText;
    //公司資料
    private static final int SingleStock_CompanyInfo_DtNo = 4005339;
    private static final String Company_info_ParamStr = "DTMode=0;DTRange=1;DTOrder=1;MajorTable=M712;MTPeriod=3;AssignID=";
    private TextView info1,info2,info3;
    //private Toolbar toolbar;



    public SingleStockFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedViewModel = ViewModelProviders.of(getActivity(), InjectorUtils.getSharedViewModelFactory(getContext())).get(SharedViewModel.class);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {

        View add_MyStock = inflater.inflate(R.layout.fragment_single_stock, container, false);
        FloatingActionMenu fb = add_MyStock.findViewById(R.id.floating_action_menu);
        FloatingActionButton fab1 = add_MyStock.findViewById(R.id.floating_action_stock);
        FloatingActionButton fab2 = add_MyStock.findViewById(R.id.floating_action_alert);
        fb.setAlpha(0.87f);
        fab1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder stockdialog = new AlertDialog.Builder(getContext());
           ForegroundColorSpan foregroundColorSpan=new ForegroundColorSpan(Color.BLACK);
            String titleText="加入自選股";
            SpannableStringBuilder ssBuilder=new SpannableStringBuilder(titleText);
            ssBuilder.setSpan(
                    foregroundColorSpan,
                    0,
                    titleText.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            );
            stockdialog.setTitle(ssBuilder).setItems(R.array.myStock_array, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                   switch (which){
                       case 0:
                           //TODO: Passing selected stockvalue to MyStock page;
                           String stockSymbol = sharedViewModel.getSelectedCurrentStock().getValue();
                           ArrayList<String> currentSavedStockList = sharedViewModel.getSavedStockList().getValue();
                           if (currentSavedStockList == null) {
                               currentSavedStockList = new ArrayList<>();
                           }
                           if (stockSymbol != null && !currentSavedStockList.contains(stockSymbol)) {
                               currentSavedStockList.add(stockSymbol);
                               sharedViewModel.setSavedStockList(currentSavedStockList);
                               saveWatchedStockListToJson(currentSavedStockList);
                           }
                           Toast.makeText(getContext(), (R.string.toast_add_mystock_successful), Toast.LENGTH_LONG).show();
//                           SharedPreferences saveMystock = getActivity().getSharedPreferences("Mystock1", Context.MODE_PRIVATE);
//                           saveMystock.edit().putString("mStock", editText.getText().toString()).commit();
                       case 1:

                   }
                    //Toast.makeText(getContext(), R.array.myStock_array[0] Toast.LENGTH_SHORT).show();
                    // The 'which' argument contains the index position
                    // of the selected item
                }
            });
            stockdialog.show();
//            //startActivity(new Intent(getActivity(), SetPrefActivity.class));
        }});

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//            AlarmSettingFragment as=new AlarmSettingFragment();
//            FragmentTransaction transaction = getsupportFragmentManager().beginTransaction();
//            transaction.replace(R.id.frame, as);
//            transaction.commitAllowingStateLoss();

                Intent setAlarmIntent = new Intent(getContext(), AlarmSettingActivity.class);
                getContext().startActivity(setAlarmIntent);
//                MainFrameActivity mainframeactivity = (MainFrameActivity) getActivity();
//                AlarmSettingFragment as = new AlarmSettingFragment();
//                mainframeactivity.setFragmentHelper(as, "AlarmSettingFragment");

                //到新fragment
                //startActivity(new Intent(getActivity(), SetPrefActivity.class));
            }
        });
        // Inflate the layout for this fragment
        return add_MyStock;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindView(view);
        //toolbar = view.findViewById(R.id.toolbar_single);
        //((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        //toolbar.setTitle();

        mCandleStickChart.setScaleYEnabled(false);
        mCandleStickChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (e instanceof CandleEntry) {
                    mStockCandleEntryTable.setVisibility(View.VISIBLE);
                    CandleEntry candleEntry = (CandleEntry) e;
                    String stockInfo = new StringBuilder()
                            .append("\nOpen: ").append(candleEntry.getOpen())
                            .append("\nClosed: ").append(candleEntry.getClose())
                            .append("\nHigh: ").append(candleEntry.getHigh())
                            .append("\nLow: ").append(candleEntry.getLow()).toString();
                    String stockInfo2 = new StringBuilder()
                            .append("Open: ").append(candleEntry.getOpen())
                            .append("\tClosed: ").append(candleEntry.getClose())
                            .append("\tHigh: ").append(candleEntry.getHigh())
                            .append("\tLow: ").append(candleEntry.getLow()).toString();
                    String mOpen = new StringBuilder("Open: ").append(candleEntry.getOpen()).toString();
                    String mClosed = new StringBuilder("Closed: ").append(candleEntry.getClose()).toString();
                    String mHigh = new StringBuilder("High: ").append(candleEntry.getHigh()).toString();
                    String mLow = new StringBuilder("Low: ").append(candleEntry.getLow()).toString();
                    mEntryOpen.setText(mOpen);
                    mEntryClosed.setText(mClosed);
                    mEntryHigh.setText(mHigh);
                    mEntryLow.setText(mLow);
                    //TODO: TOAST StockINFO
                    //Toast.makeText(getContext(), stockInfo, Toast.LENGTH_SHORT).show();
                    //mtextView.setText(stockInfo2);
                }
            }

            @Override
            public void onNothingSelected() {
                mStockCandleEntryTable.setVisibility(View.GONE);
            }
        });
//        setChart(mCandleStickChart, generateMockupData());
//        getStockData(stockServiceUrl, "AAPL");

        //TODO: Observe LiveData
        sharedViewModel.getSelectedCurrentStock().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String stockSymbol) {
                if (stockSymbol == null) {
                    stockSymbol = "DDD";
                }
                getStockData(stockServiceUrl, stockSymbol);
            }
        });
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stockSymbol = editText.getText().toString();
                if (stockSymbol.equals("")) {
                    return;
                }
                Log.d("SingleStock", stockSymbol);
                sharedViewModel.selectCurrentStock(stockSymbol);
            }
        });
    }

    private void saveWatchedStockListToJson(ArrayList<String> savedStockSymbolList) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(PreferencesDataName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(savedStockSymbolList);
        System.out.println(json);
        editor.putString(TAG_STOCK_LIST, json);
        editor.apply();
    }

    private void bindView(View view) {
        //CandleStickChart
        mCandleStickChart = view.findViewById(R.id.candle_stick_chart);

        //Selected CandleEntry TextView Table
        mStockCandleEntryTable = view.findViewById(R.id.table_selected_entry);
        mEntryOpen = view.findViewById(R.id.txt_EntryOpen);
        mEntryClosed = view.findViewById(R.id.txt_EntryClosed);
        mEntryHigh = view.findViewById(R.id.txt_EntryHigh);
        mEntryLow = view.findViewById(R.id.txt_EntryLow);

        //TODO: remove these button after testing
        btn_update = view.findViewById(R.id.btn_chartupdate);
        editText = view.findViewById(R.id.input_txt);

        //季財報
        tv1 = view.findViewById(R.id.profit_marginQ);
        tv2 = view.findViewById(R.id.net_pmQ);
        tv3 = view.findViewById(R.id.operating_pmQ);
        tv4 = view.findViewById(R.id.roeQ);
        tv5 = view.findViewById(R.id.current_ratioQ);
        tv6 = view.findViewById(R.id.inventory_turnoverQ);
        tv7 = view.findViewById(R.id.asset_turnoverQ);
        tv8 = view.findViewById(R.id.dateQ);

        //年財報
        tv9 = view.findViewById(R.id.profit_marginY);
        tv10 = view.findViewById(R.id.net_pmY);
        tv11 = view.findViewById(R.id.operating_pmY);
        tv12 = view.findViewById(R.id.roeY);
        tv13 = view.findViewById(R.id.current_ratioY);
        tv14 = view.findViewById(R.id.inventory_turnoverY);
        tv15 = view.findViewById(R.id.asset_turnoverY);
        tv16 = view.findViewById(R.id.dateY);

        //公司資訊
        info1 = view.findViewById(R.id.company);
        info2 = view.findViewById(R.id.industry);
        info3 = view.findViewById(R.id.price);

        //Add to list
        tvList.add(tv1);
        tvList.add(tv2);
        tvList.add(tv3);
        tvList.add(tv4);
        tvList.add(tv5);
        tvList.add(tv6);
        tvList.add(tv7);
        tvList.add(tv8);
        tvList.add(tv9);
        tvList.add(tv10);
        tvList.add(tv11);
        tvList.add(tv12);
        tvList.add(tv13);
        tvList.add(tv14);
        tvList.add(tv15);
        tvList.add(tv16);
    }

    private void setChart(CandleStickChart candleStickChart, CandleData candleData) {
        candleStickChart.setHighlightPerDragEnabled(true);
        candleStickChart.setDrawBorders(true);
        candleStickChart.setBorderColor(getResources().getColor(R.color.LightSlateGray));
        YAxis yAxis = candleStickChart.getAxisLeft();
        YAxis rightAxis = candleStickChart.getAxisRight();
        yAxis.setDrawGridLines(false);
        rightAxis.setDrawGridLines(false);
        candleStickChart.requestDisallowInterceptTouchEvent(true);
        yAxis.setTextColor(Color.WHITE);


        XAxis xAxis = candleStickChart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        rightAxis.setTextColor(Color.WHITE);
        yAxis.setDrawLabels(true);
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setTextColor(Color.WHITE);


        Legend legend = candleStickChart.getLegend();
        legend.setEnabled(false);

        // CandleData data = generateMockupData();
        candleStickChart.setDescription(setDescription());
        candleStickChart.setData(candleData);
        candleStickChart.invalidate();
    }

    private CandleData generateMockupData() {
        ArrayList<CandleEntry> yValsCandleStick = new ArrayList<CandleEntry>();
        yValsCandleStick.add(new CandleEntry(0f, 225.0f, 219.84f, 224.94f, 221.07f));
        yValsCandleStick.add(new CandleEntry(1f, 228.35f, 222.57f, 223.52f, 226.41f));
        yValsCandleStick.add(new CandleEntry(2f, 226.84f, 222.52f, 225.75f, 223.84f));
        yValsCandleStick.add(new CandleEntry(3f, 222.95f, 217.27f, 222.15f, 217.88f));
        yValsCandleStick.add(new CandleEntry(4f, 223.95f, 220.27f, 225.15f, 221.88f));
        yValsCandleStick.add(new CandleEntry(5f, 224.95f, 191.27f, 221.15f, 195.88f));


        CandleDataSet set1 = new CandleDataSet(yValsCandleStick, "DataSet1");
        set1.setColor(Color.rgb(80, 80, 80));
        set1.setShadowColor(getResources().getColor(R.color.LightSlateGray));
        set1.setShadowWidth(0.8f);
        set1.setDecreasingColor(getResources().getColor(R.color.Red));
        set1.setDecreasingPaintStyle(Paint.Style.FILL);
        set1.setIncreasingColor(getResources().getColor(R.color.Green));
        set1.setIncreasingPaintStyle(Paint.Style.FILL);
        set1.setNeutralColor(Color.LTGRAY);
        set1.setDrawValues(true);

        CandleData data = new CandleData(set1);
        return data;
    }

    private CandleData InputYDataSet(ArrayList<CandleEntry> Y_Polt_Point) {
        CandleDataSet set1 = new CandleDataSet(Y_Polt_Point, "DataSet1");
        set1.setColor(Color.rgb(80, 80, 80));
        set1.setValueTextColor(getResources().getColor(R.color.White));
        set1.setShadowColor(getResources().getColor(R.color.LightSlateGray));
        set1.setShadowWidth(0.8f);
        set1.setDecreasingColor(getResources().getColor(R.color.Red));
        set1.setDecreasingPaintStyle(Paint.Style.FILL);
        set1.setIncreasingColor(getResources().getColor(R.color.Green));
        set1.setIncreasingPaintStyle(Paint.Style.FILL);
        set1.setNeutralColor(Color.LTGRAY);
        set1.setDrawValues(true);

        CandleData data = new CandleData(set1);
        return data;
    }


    private Description setDescription() {
        Description description = new Description();
        description.setText("NASDAQ: AAPL");
        description.setTextColor(Color.WHITE);
        return description;
    }

    private void setDescription(CandleStickChart candleStickChart, String Sticker) {
        Description description = new Description();
        description.setText(Sticker);
        description.setTextColor(Color.WHITE);
        candleStickChart.setDescription(description);
    }

    private void getStockData(String serviceUrl, String stockID) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Log.d("DataSet", SingleStock_Daily_ParamStr);

        Call<SingleStockDataResponse> StockDataResponseCall = jsonPlaceHolderApi.getSingleStock(
                SingleStock_Action, SingleStock_Daily_DtNo, SingleStock_Daily_ParamStr + stockID, SingleStock_FilterNo, SingleStock_KeyMap, SingleStock_AssignSPID
        );

        //財報
        Call<SingleStockDataResponse> StockFinancialReportCall = jsonPlaceHolderApi.getSingleStock(
                SingleStock_Action, SingleStock_Quartly_DtNo, SingleStock_FnReprt_ParamStr + stockID, SingleStock_FilterNo, SingleStock_KeyMap, SingleStock_AssignSPID
        );

        //公司資訊
        Call<SingleStockDataResponse> StockCompanyInfoCall = jsonPlaceHolderApi.getSingleStock(
                SingleStock_Action, SingleStock_CompanyInfo_DtNo, Company_info_ParamStr + stockID, SingleStock_FilterNo, SingleStock_KeyMap, SingleStock_AssignSPID
        );


        //TODO: REFACTOR
        StockDataResponseCall.enqueue(new Callback<SingleStockDataResponse>() {
            @Override
            public void onResponse(Call<SingleStockDataResponse> call, Response<SingleStockDataResponse> response) {

                SingleStockDataResponse stockDataResponse = response.body();
                mRetrofitLoginTaskRunning = false;

                if (!response.isSuccessful()) {
                    Log.d("Login", ("Code: " + response.code()));
                    //Toast.makeText(getActivity(), "Error Response Code: " + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }
                //TODO: Parsing plot point data into graph
                Log.d("SingleStock", "DataSize" + stockDataResponse.getDataSet().size());
                if (stockDataResponse.getDataSet().size() < 5) {
                    return;
                }
                //getTitle Return an array list
                drawCandleStickChart(stockDataResponse.getTitle(), stockDataResponse.getDataSet());
                Log.d("DataSet", stockDataResponse.getTitle().toString());
                //Toast.makeText(getActivity(), "YEA Response Code: " + response.code(), Toast.LENGTH_SHORT).show();

//                switch (stockDataResponse.getResponseCode()){
//                    case MemberProfile_ResponseeCode_Success:
//                        break;
//                    case MemberProfile_ResponseeCode_AuthedFail:
//                        //TODO: REFACTOR THIS LOGIC
//                        break;
//                    default:
//                        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT);
//                        break;
            }

            @Override
            public void onFailure(Call<SingleStockDataResponse> call, Throwable t) {
                Log.d("Login", ("Failed: " + t.toString()));
                Toast.makeText(getActivity(), "Unable to connect to the server", Toast.LENGTH_SHORT).show();
            }
        });

        //財報
        StockFinancialReportCall.enqueue(new Callback<SingleStockDataResponse>() {
            @Override
            public void onResponse(Call<SingleStockDataResponse> call, Response<SingleStockDataResponse> response) {
                SingleStockDataResponse stockDataResponse = response.body();
                mRetrofitLoginTaskRunning = false;

                if (!response.isSuccessful()) {
                    Log.d("Login", ("Code: " + response.code()));
                    Toast.makeText(getActivity(), "Error Response Code: " + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }
                setFinancialReportData(stockDataResponse.getTitle(), stockDataResponse.getDataSet());
            }

            @Override
            public void onFailure(Call<SingleStockDataResponse> call, Throwable t) {
                Log.d("Login", ("Failed: " + t.toString()));
                Toast.makeText(getActivity().getApplicationContext(), "Unable to connect to the server", Toast.LENGTH_SHORT).show();
            }
        });

        //公司資訊
        StockCompanyInfoCall.enqueue(new Callback<SingleStockDataResponse>() {
            @Override
            public void onResponse(Call<SingleStockDataResponse> call, Response<SingleStockDataResponse> response) {
                SingleStockDataResponse stockDataResponse = response.body();
                mRetrofitLoginTaskRunning = false;

                if (!response.isSuccessful()) {
                    Log.d("Login", ("Code: " + response.code()));
                    Toast.makeText(getActivity(), "Error Response Code: " + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }
                setCompanyInfoData(stockDataResponse.getTitle(), stockDataResponse.getDataSet());
            }

            @Override
            public void onFailure(Call<SingleStockDataResponse> call, Throwable t) {
                Log.d("Login", ("Failed: " + t.toString()));
                Toast.makeText(getActivity(), "Unable to connect to the server", Toast.LENGTH_SHORT).show();
            }
        });

    }

    //TODO: Remove unnecessary  params
    private void drawCandleStickChart(ArrayList<String> titles, ArrayList<ArrayList<String>> dataArr) {
        ArrayList<CandleEntry> plot_point = new ArrayList<>();
        ArrayList<String> xLabels = new ArrayList<>();
        final int index_tagDate = 0;
        final int index_date = 1;
        final int index_TickerSymbol = 2;
        final int index_name = 3;
        final int index_open = 4;
        final int index_high = 5;
        final int index_low = 6;
        final int index_close = 7;
        final int index_volumn = 8;
        int loopIndex = 0;
        String stockName = null;
        String tickerSymbol = null;
        for (ArrayList<String> plot : dataArr) {
            if (stockName == null || tickerSymbol == null) {
                tickerSymbol = String.valueOf(plot.get(2));
                stockName = String.valueOf(plot.get(3));
            }

            String dateStr = plot.get(index_date);
            float open = Float.parseFloat(String.valueOf(plot.get(index_open)));
            float high = Float.parseFloat(String.valueOf(plot.get(index_high)));
            float low = Float.parseFloat(String.valueOf(plot.get(index_low)));
            float close = Float.parseFloat(String.valueOf(plot.get(index_close)));
            DateFormat parser = new SimpleDateFormat("yyyyMMdd");
            DateFormat formatter = new SimpleDateFormat("MM/dd");
            try {
                Date convertDate = parser.parse(dateStr);
                String output = formatter.format(convertDate);
                xLabels.add(output);
            } catch (ParseException e) {
                e.printStackTrace();
                continue;
            }
            //float date = Math.round(Float.parseFloat(plot.get(index_date)));
            plot_point.add(new CandleEntry(loopIndex, high, low, open, close));
            loopIndex++;
        }

        CandleData candleData = InputYDataSet(plot_point);
        setChart(mCandleStickChart, candleData);
        setDescription(mCandleStickChart, tickerSymbol);
        setAxisFormatter(mCandleStickChart, xLabels);
    }

    private void setAxisFormatter(XAxis xAxis, final ArrayList<String> xLabels) {
        xAxis.setValueFormatter(new IndexAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                int index = (int) value % xLabels.size();
                Log.d("Plotting", String.valueOf(index));
                return xLabels.get(index);
                //return super.getFormattedValue(value, axis);
            }
        });
    }

    private void setAxisFormatter(CandleStickChart candleStickChart, final ArrayList<String> xLabels) {
        XAxis xAxis = candleStickChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                int index = (int) value % xLabels.size();
                Log.d("Plotting", String.valueOf(index));
                return xLabels.get(index);
                //return super.getFormattedValue(value, axis);
            }
        });
    }

    //財報
    private void setFinancialReportData(ArrayList<String> titles, ArrayList<ArrayList<String>> dataArr) {

        tv1.setText(dataArr.get(3).get(1));
        tv2.setText(dataArr.get(3).get(2));
        tv3.setText(dataArr.get(3).get(3));
        tv4.setText(dataArr.get(3).get(4));
        tv5.setText(dataArr.get(3).get(5));
        tv6.setText(dataArr.get(3).get(6));
        tv7.setText(dataArr.get(3).get(7));
        tv8.setText(dataArr.get(3).get(0));

        for (int i = 0; i < dataArr.size(); i++) {
            String test = dataArr.get(i).get(0);
            String DateCompare = test.substring(test.length() - 1);
            String DateKey = "4";

            if (DateCompare.equals(DateKey)) {
                tv9.setText(dataArr.get(i).get(1));
                tv10.setText(dataArr.get(i).get(2));
                tv11.setText(dataArr.get(i).get(3));
                tv12.setText(dataArr.get(i).get(4));
                tv13.setText(dataArr.get(i).get(5));
                tv14.setText(dataArr.get(i).get(6));
                tv15.setText(dataArr.get(i).get(7));
                tv16.setText(dataArr.get(i).get(0));
            }
        }

        //Validate Text
        for (TextView tv : tvList) {
            String text = tv.getText().toString();
            tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            if (text.isEmpty()) {
                tv.setText("-");
            }
        }
    }

    //公司資訊
    private void setCompanyInfoData(ArrayList<String> titles, ArrayList<ArrayList<String>> dataArr){
        //代號
        info1.setText(dataArr.get(0).get(3));
        info2.setText(dataArr.get(0).get(23));
        info3.setText(dataArr.get(0).get(7));
       // toolbar.setTitle(dataArr.get(0).get(1));
    }
}