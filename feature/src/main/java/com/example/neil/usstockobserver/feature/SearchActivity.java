package com.example.neil.usstockobserver.feature;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.neil.usstockobserver.feature.Leaderboard.Data.network.NetworkUtils;
import com.example.neil.usstockobserver.feature.Leaderboard.InjectorUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import android.support.v7.widget.SearchView;

public class SearchActivity extends AppCompatActivity {


    //搜尋
    Boolean ready = false;
    ListView listView;
    ArrayList<String> list;
    ArrayAdapter<String> adapter;
    boolean mIsSearch = false;
    private static final String WholeStock_Action = "getdtnodata";
    private static final int WholeStock_Info_DtNo = 4511001;
    private static final String WholeStock_ParamStr = "SPMode=0;DTMode=0;";
    private static final int WholeStock_FilterNo = 0;
    private static final String WholeStock_KeyMap = "";
    private static final String WholeStock_AssignSPID = "";
    private final String stockServiceUrl = "https://www.cmoney.tw/MobileService/ashx/";
    private SharedViewModel sharedViewModel;
    private Intent mStockIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        mStockIntent = new Intent(getBaseContext(), SingleStockActivity.class);
        sharedViewModel = ViewModelProviders.of(this, InjectorUtils.getSharedViewModelFactory(getApplicationContext())).get(SharedViewModel.class);
        sharedViewModel.selectCurrentStock("AMZN");


        Toolbar toolbar = findViewById(R.id.toolbar_search);
        setSupportActionBar(toolbar);
        toolbar.setTitle("搜尋");

//        toolbar.inflateMenu(R.menu.toolbar_menu);
//        Menu menu = toolbar.getMenu();
//        MenuItem item = menu.findItem(R.id.stock_search);
//        item.setVisible(false);



        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.searchviewList);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = parent.getAdapter().getItem(position).toString();
                String[] s1 = s.split("\\s+/");
                Toast.makeText(view.getContext(), s1[0], Toast.LENGTH_SHORT).show();
                sharedViewModel.selectCurrentStock(s1[0]);
                startActivity(mStockIntent);
            }
        });

//        list = new ArrayList<>();
//        list.add("Apple");
//        list.add("Banana");
//        list.add("Pineapple");
//        list.add("Orange");
//        list.add("Lychee");
//        list.add("Gavava");
//        list.add("Peech");
//        list.add("Melon");
//        list.add("Watermelon");
//        list.add("Papaya");



        SearchView stockSearchview = (SearchView) findViewById(R.id.searchView);
        stockSearchview.setSubmitButtonEnabled(true);
        stockSearchview.setQueryHint("請輸入股票代碼/公司名稱");

        getStockData(stockServiceUrl);

        stockSearchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!ready){
                    listView.setAdapter(null);    //還沒資料進來時的開關
                    return true;
                }
                if (!mIsSearch && s.length() != 0) { //搜尋框有值時
                    listView.setAdapter(adapter);
                    mIsSearch = true;
                } else if (mIsSearch && s.length() == 0) { //搜尋框是空的時
                    listView.setAdapter(null);
                    mIsSearch = false;
                }
                if (mIsSearch) { //過濾Adapter的內容
                    adapter.getFilter().filter(s);
//                            Filter filter = mSearchAdapter.getFilter();
//                            filter.filter(newText);
                }
                return true;

            }
        });
    }


    private void getStockData(String serviceUrl) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<WholeStockResponse> wholeStockResponseCall = jsonPlaceHolderApi.getWholeStock(
                WholeStock_Action, WholeStock_Info_DtNo ,WholeStock_ParamStr, WholeStock_FilterNo, WholeStock_KeyMap, WholeStock_AssignSPID
        );


        wholeStockResponseCall.enqueue(new Callback<WholeStockResponse>() {
            @Override
            public void onResponse(Call<WholeStockResponse> call, Response<WholeStockResponse> response) {
                WholeStockResponse wholeStockResponse = response.body();

                ArrayList<String> titles = wholeStockResponse.getTitle();
                //TODO: Temp Solution for filtering out ETF and Funds
                ArrayList<ArrayList<String>> dataSet = NetworkUtils.convertToStockDataSet(wholeStockResponse);
                //ArrayList<ArrayList<String>> dataSet = wholeStockResponse.getDataSet();
                getWholeStockData(titles, dataSet);
            }

            @Override
            public void onFailure(Call<WholeStockResponse> call, Throwable t) {

            }

        });
    }

    public void getWholeStockData(ArrayList<String> titles, ArrayList<ArrayList<String>> dataSet){
        list = new ArrayList<>();

        //TODO: Need to filter out ETF and FUNDs
        for(int i=0;i<dataSet.size();i++){
            list.add(dataSet.get(i).get(0)+ " / "+dataSet.get(i).get(1));
        }
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,list);
        listView.setAdapter(adapter);
        ready = true;
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(SearchActivity.this, "Click item" + position, Toast.LENGTH_SHORT).show();
//            }
//        });



//                for (ArrayList<String> data: dataSet) {
//                    list.add(dataSet.get(0).get(0));
//
//                }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.toolbar_menu, menu);

//        MenuItem searchItem = menu.findItem(R.id.searchView);
//        SearchView stockSearchview = (SearchView) searchItem.getActionView();
//        stockSearchview.setSubmitButtonEnabled(true);
//        stockSearchview.setQueryHint("請輸入股票代碼");
//
//        stockSearchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                if (!mIsSearch && newText.length() != 0) { //搜尋框有值時
//                    listView.setAdapter(adapter);
//                    mIsSearch = true;
//                } else if (mIsSearch && newText.length() == 0) { //搜尋框是空的時
//                    listView.setAdapter(null);
//                    mIsSearch = false;
//                }
//                if (mIsSearch) { //過濾Adapter的內容
//                    adapter.getFilter().filter(newText);
////                    Filter filter = mSearchAdapter.getFilter();
////                    filter.filter(newText);
//                }
//                return true;
//
//
////                adapter.getFilter().filter(newText);
////                return false;
//            }
//        });
//        MenuItem menuItem = menu.findItem(R.id.action_share);
//        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);
//        setShareActionIntent("Want to join me for pizza?");
        return super.onCreateOptionsMenu(menu);
    }
}



