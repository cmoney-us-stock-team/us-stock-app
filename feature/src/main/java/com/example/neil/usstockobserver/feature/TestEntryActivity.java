package com.example.neil.usstockobserver.feature;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class TestEntryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_entry);
    }

    @Override
    protected void onStart() {
        super.onStart();

        addButtonClick();

    }

    private void addButtonClick(){
        findViewById(R.id.page1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestEntryActivity.this, LoginActivity.class);
                startActivity(intent);
                /* Don't use finish() here since we want the up button (the arrow on top) to return to this activity */
                //finish();
            }
        });

        findViewById(R.id.page3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestEntryActivity.this, MainFrameActivity.class);
                startActivity(intent);
                /* Don't use finish() here since we want the up button (the arrow on top) to return to this activity */
                //finish();
            }
        });

        findViewById(R.id.page2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestEntryActivity.this, SingleStockActivity.class);
                startActivity(intent);
            }
        });
    }
}
