package com.example.neil.usstockobserver.feature;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgetpasswordActivity extends AppCompatActivity {

    private final String loginServiceUrl = "https://www.cmoney.tw/MobileService/ashx/";
    private static final int AppID = 2;
    private static final int Device_Android = 2;
    private static final boolean Need_Broadcast = false;
    private static final String Broadcast_Token = "";
    private static final String Action_DefaultLogin = "getloginguid";
    private static final String Action_Forgot_Password = "forgetpassword";
    private static final String Action_Register_Account = "registeraccount";
    private static final String Action_Get_MemberProfile = "getmemberprofile";
    private static final int Login_Response_Successful = 1;
    private static final int Login_Response_AuthedFail = 2;
    private static final int Register_Response_Successful = 1;
    private static final int Register_Response_Duplicate = 4;
    private static final int Register_Response_Duplicate2 = 6;
    private static final int Register_Response_Duplicate3 = 11;
    private static final int Login_Response_ServerNoResponse = 9002;

    private static final String TAG = "ForgetPasswordActivity";

    ProgressDialog mProgressDialog;

    EditText _emailText;
    //EditText _passwordText;
    //EditText _reEnterPasswordText;
    Button _forgetPasswordButton;
    //TextView _loginLink;
    private View mForgetPasswordFormView;
    private View mForgetPasswordProgressView;

    private void bindView(){
        _emailText = findViewById(R.id.forgetPW_email);
//        _passwordText = findViewById(R.id.input_password);
//        _reEnterPasswordText = findViewById(R.id.input_confirm_password);
//        _signupButton = findViewById(R.id.btn_signup);
//        _loginLink = findViewById(R.id.link_login);
        mForgetPasswordFormView = findViewById(R.id.forgetPassword_form);
        mForgetPasswordProgressView = findViewById(R.id.forgetPassword_progress);
        _forgetPasswordButton = findViewById(R.id.btn_forgetPassword);
        mProgressDialog = new ProgressDialog(ForgetpasswordActivity.this,
                com.example.neil.usstockobserver.R.style.AppTheme_Dark_Dialog);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getString(R.string.prompt_success_forgetPassword));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpassword);
        bindView();
        //ButterKnife.bind(this);

        _forgetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

    }

    public void signup() {
        //TODO: Add Api Logics
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        _forgetPasswordButton.setEnabled(false);


//        String name = _nameText.getText().toString();
//        String address = _addressText.getText().toString();
       String email = _emailText.getText().toString();
//        String mobile = _mobileText.getText().toString();
        //String password = _passwordText.getText().toString();
        //String reEnterPassword = _reEnterPasswordText.getText().toString();

        // TODO: Implement your own signup logic here.
        showProgress(true);
        signupApiRequest(loginServiceUrl, email);

//        new android.os.Handler().postDelayed(
//                new Runnable() {
//                    public void run() {
//                        // On complete call either onSignupSuccess or onSignupFailed
//                        // depending on success
//                        onSignupSuccess();
//                        // onSignupFailed();
//                        progressDialog.dismiss();
//                    }
//                }, 3000);
    }

    public void onSignupSuccess() {
        _forgetPasswordButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        _forgetPasswordButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

//        String name = _nameText.getText().toString();
//        String address = _addressText.getText().toString();
        String email = _emailText.getText().toString();
//        String mobile = _mobileText.getText().toString();
       // String password = _passwordText.getText().toString();
       // String reEnterPassword = _reEnterPasswordText.getText().toString();

//        if (name.isEmpty() || name.length() < 3) {
//            _nameText.setError("at least 3 characters");
//            valid = false;
//        } else {
//            _nameText.setError(null);
//        }
//
//        if (address.isEmpty()) {
//            _addressText.setError("Enter Valid Address");
//            valid = false;
//        } else {
//            _addressText.setError(null);
//        }


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

//        if (mobile.isEmpty() || mobile.length()!=10) {
//            _mobileText.setError("Enter Valid Mobile Number");
//            valid = false;
//        } else {
//            _mobileText.setError(null);
//        }

        return valid;
    }

//    public String MD5(String md5) {
//        try {
//            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
//            byte[] array = md.digest(md5.getBytes("UTF-8"));
//            StringBuffer sb = new StringBuffer();
//            for (int i = 0; i < array.length; ++i) {
//                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
//            }
//            return sb.toString();
//        } catch (java.security.NoSuchAlgorithmException e) {
//        } catch (UnsupportedEncodingException ex) {
//        }
//        return null;
//    }

    private void signupApiRequest(String serviceUrl, String account) {

        //TODO: Refactor it  into Singleton mode
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<ForgetPasswordResponse> forgetPasswordResponseCall = jsonPlaceHolderApi.getForgetpassward(
                Action_Forgot_Password, account);

        //TODO: REFACTOR
        forgetPasswordResponseCall.enqueue(new Callback<ForgetPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgetPasswordResponse> call, Response<ForgetPasswordResponse> response) {
                Intent intent = null;
                ForgetPasswordResponse forgetPasswordResponse = response.body();
                showProgress(false);

                if (!response.isSuccessful()) {
                    Log.d("Login", ("Code: " + response.code()));
                    Toast.makeText(getApplicationContext(), "Error Response Code: " + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }

                switch (forgetPasswordResponse.getResponseCode()) {
                    case Register_Response_Successful:
                        Toast.makeText(getApplicationContext(), "Register successfully", Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(), LoginActivity.class);
                        break;
                    case Register_Response_Duplicate:
                    case Register_Response_Duplicate2:
                    case Register_Response_Duplicate3:
                        //TODO: REFACTOR THIS LOGIC
                        _emailText.setError(getString(R.string.error_account_exist));
                        _emailText.requestFocus();
                        break;
                    default:
                        Toast.makeText(getApplicationContext(), "Connection problem occurred,\nplease try again later", Toast.LENGTH_SHORT);
                        break;
                }
                _forgetPasswordButton.setEnabled(true);
               // Toast.makeText(getApplicationContext(), "HTTP Response Code: " + response.code() + "\n" + loginResponse.getAuthToken(), Toast.LENGTH_LONG).show();
                if (intent != null) {
                    //TODO: Change Pop-up dialog with a tick mark.
                    mProgressDialog.show();
                    //TODO: Add animation delay
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ForgetPasswordResponse> call, Throwable t) {
                Log.d("Login", ("Failed: " + t.toString()));
                Toast.makeText(getApplicationContext(), "Uable to connect to the server", Toast.LENGTH_SHORT).show();
                showProgress(false);
                onSignupFailed();
            }
        });
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mForgetPasswordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mForgetPasswordFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mForgetPasswordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mForgetPasswordProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mForgetPasswordProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mForgetPasswordProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mForgetPasswordProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mForgetPasswordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
